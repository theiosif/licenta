package com.iosif.dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iosif.entity.Domeniu;
import com.iosif.entity.Specializare;

@Repository
public class DomspecDAOImpl implements DomspecDAO {

	@Autowired
	private SessionFactory sessionFactory;

        
        // --> Domenii <-- \\
	@Override
	public List<Domeniu> getDomenii() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Domeniu> cq = cb.createQuery(Domeniu.class);
		Root<Domeniu> root = cq.from(Domeniu.class);
		cq.select(root);
		Query query = session.createQuery(cq);
		return query.getResultList();
	}

	@Override
	public void deleteDomeniu(int id) {
		Session session = sessionFactory.getCurrentSession();
		Domeniu book = session.byId(Domeniu.class).load(id);
		session.delete(book);
	}

	@Override
	public void saveDomeniu(Domeniu theDomeniu) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(theDomeniu);
	}

	@Override
	public Domeniu getDomeniu(int theId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Domeniu theDomeniu = currentSession.get(Domeniu.class, theId);
		return theDomeniu;
	}
        
        
        
        // --> Specializari <-- \\
        @Override
	public List<Specializare> getSpecializari() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Specializare> cq = cb.createQuery(Specializare.class);
		Root<Specializare> root = cq.from(Specializare.class);
		cq.select(root);
		Query query = session.createQuery(cq);
		return query.getResultList();
	}

	@Override
	public void deleteSpecializare(int id) {
		Session session = sessionFactory.getCurrentSession();
		Specializare book = session.byId(Specializare.class).load(id);
		session.delete(book);
	}

	@Override
	public void saveSpecializare(Specializare theSpecializare) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(theSpecializare);
	}

	@Override
	public Specializare getSpecializare(int theId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Specializare theSpecializare = currentSession.get(Specializare.class, theId);
		return theSpecializare;
	}
}

