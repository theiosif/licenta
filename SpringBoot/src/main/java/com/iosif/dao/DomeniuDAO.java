package com.iosif.dao;

import com.iosif.entity.Domeniu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author yoji
 */

@Repository
public interface DomeniuDAO extends JpaRepository<Domeniu, Short> {

}