package com.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.model.*;
import com.util.DBConnection;


/**
 *
 * @author yoji
 */

public class DisciplinaDAO {
     
    public static List<Disciplina> getAllRecords(){
            List<Disciplina> list=new ArrayList<Disciplina>();

            try{
                    Connection con=DBConnection.getConnection();
                    
                    PreparedStatement ps=con.prepareStatement("SELECT ID_DISCIPLINA, COD_1, COD_2, COD_3, COD_4, COD_5, ID_DOMENIU, ID_SPECIALIZARE, NUME_DISCIPLINA, ECTS, TIP_EXAMINARE FROM discipline ORDER BY COD_3");
                    
                    // COD_3        <--> Semestru
                    // (COD_3+1)%2  <--> An
                    
                    ResultSet rs=ps.executeQuery();
                    System.out.println("EXECUTING..." + ps.toString());
                    while(rs.next()){
                            System.out.println("EXECUTING...");
                            Disciplina u=new Disciplina();
                            u.setIdDisciplina(rs.getInt("ID_DISCIPLINA"));
                            u.setCod1(rs.getString("COD_1"));
                            u.setCod2(rs.getString("COD_2").charAt(0));
                            u.setCod3(rs.getString("COD_3"));
                            u.setCod4(rs.getString("COD_4").charAt(0));
                            u.setCod5(rs.getString("COD_5"));                            
                            u.setIdSpecializare((short)rs.getInt("ID_DOMENIU"));
                            u.setIdSpecializare((short)rs.getInt("ID_SPECIALIZARE"));
                            u.setNumeDisciplina(rs.getString("NUME_DISCIPLINA"));
                            u.setEcts((short)rs.getInt("ECTS"));
                            u.setTipExaminare(rs.getString("TIP_Examinare"));
                            list.add(u);
                    }
                    
                    DBConnection.disconnect(con);
            }catch(Exception e){System.out.println(e);}
            return list;
    }
     
    public boolean insertDisciplina(Disciplina d) throws SQLException {
        String sql = "INSERT INTO `discipline`"
                   + "(ID_DOMENIU, ID_SPECIALIZARE, "
                   + "COD_1, COD_2, COD_3, COD_4, COD_5, "
                   + "NUME_DISCIPLINA, LB_PREDARE, ECTS, "
                   + "ORE_SAPT_C, ORE_SAPT_S, ORE_SAPT_L, ORE_SAPT_P, ORE_SAPT_PW, "
                   + "SAPTAMANI, TIP_EXAMINARE"
                   + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection con=DBConnection.getConnection();
         
        PreparedStatement statement = con.prepareStatement(sql);
        
        statement.setInt(1, d.getIdDomeniu());
        statement.setInt(2, d.getIdSpecializare());
        statement.setString(3, d.getCod1());
        statement.setString(4, String.valueOf(d.getCod2()));
        statement.setString(5, d.getCod3());
        statement.setString(6, String.valueOf(d.getCod4()));
        statement.setString(7, d.getCod5());
        statement.setString(8, d.getNumeDisciplina());
        statement.setString(9, d.getLbPredare());
        statement.setInt(10, d.getEcts());
        statement.setInt(11, d.getOreSaptC());
        statement.setInt(12, d.getOreSaptS());
        statement.setInt(13, d.getOreSaptL());                
        statement.setInt(14, d.getOreSaptP());
        statement.setInt(15, d.getOreSaptPw());
        statement.setInt(15, d.getSaptamani());
        statement.setString(16, d.getTipExaminare());
                        
        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        DBConnection.disconnect(con);
        return rowInserted;
    }
     
    public boolean delDisciplina(int id) throws SQLException {
        String sql = "DELETE FROM `discipline` WHERE ID=?";
        Connection con=DBConnection.getConnection();
         
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setInt(1, id);
                        
        boolean rowDel = statement.executeUpdate() == 1;
        statement.close();
        DBConnection.disconnect(con);
        return rowDel;
    }
     
    public Disciplina getDisciplinaa(int id) throws SQLException{
        String sql = "SELECT * FROM `discipline` WHERE ID=?";
        Connection con=DBConnection.getConnection();
         
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, id);
        
        int rowSel = 0;
        Disciplina selected=new Disciplina();
        
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            System.out.println("EXECUTING...");
            
            // TODO: add toate campurile
            selected.setIdDisciplina(rs.getInt("ID_DISCIPLINA"));
            selected.setCod1(rs.getString("COD_1"));
            selected.setCod2(rs.getString("COD_2").charAt(0));
            selected.setCod3(rs.getString("COD_3"));
            selected.setCod4(rs.getString("COD_4").charAt(0));
            selected.setCod5(rs.getString("COD_5"));                            
            selected.setIdSpecializare((short)rs.getInt("ID_DOMENIU"));
            selected.setIdSpecializare((short)rs.getInt("ID_SPECIALIZARE"));
            selected.setNumeDisciplina(rs.getString("NUME_DISCIPLINA"));
            selected.setEcts((short)rs.getInt("ECTS"));
            selected.setTipExaminare(rs.getString("TIP_Examinare"));
            rowSel++;
        }
        ps.close();
        DBConnection.disconnect(con);
        
        if (rowSel != 1){
            System.out.println("get failed. Invalid id maybe?");
        }
        return selected;
    }
}