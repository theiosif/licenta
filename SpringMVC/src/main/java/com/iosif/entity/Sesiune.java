/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iosif.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yoji
 */
@Entity
@Table(name = "sesiuni")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sesiune.findAll", query = "SELECT s FROM Sesiune s"),
    @NamedQuery(name = "Sesiune.findByIdSesiune", query = "SELECT s FROM Sesiune s WHERE s.idSesiune = :idSesiune"),
    @NamedQuery(name = "Sesiune.findByCreated", query = "SELECT s FROM Sesiune s WHERE s.created = :created")})
public class Sesiune implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 21)
    @Column(name = "ID_SESIUNE")
    private String idSesiune;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @JoinColumn(name = "ID_UTILIZATOR", referencedColumnName = "ID_UTILIZATOR")
    @ManyToOne
    private Utilizator idUtilizator;

    public Sesiune() {
    }

    public Sesiune(String idSesiune) {
        this.idSesiune = idSesiune;
    }

    public Sesiune(String idSesiune, Date created) {
        this.idSesiune = idSesiune;
        this.created = created;
    }

    public String getIdSesiune() {
        return idSesiune;
    }

    public void setIdSesiune(String idSesiune) {
        this.idSesiune = idSesiune;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Utilizator getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(Utilizator idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSesiune != null ? idSesiune.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sesiune)) {
            return false;
        }
        Sesiune other = (Sesiune) object;
        if ((this.idSesiune == null && other.idSesiune != null) || (this.idSesiune != null && !this.idSesiune.equals(other.idSesiune))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.Sesiuni[ idSesiune=" + idSesiune + " ]";
    }
    
}
