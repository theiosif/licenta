package com.iosif.service;

@Service("UtilizatorService")
public class UtilizatorService {

    private UtilizatorDAO UtilizatorDAO;
    private AuthorityDAO AuthorityDAO;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UtilizatorService(UtilizatorDAO UtilizatorDAO,
                       AuthorityDAO AuthorityDAO,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.UtilizatorDAO = UtilizatorDAO;
        this.AuthorityDAO = AuthorityDAO;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public Utilizator findUtilizatorByNume(String nume) {
        return UtilizatorDAO.findByNume(nume);
    }

    public void saveUtilizator(Utilizator Utilizator) {
        Utilizator.setParola(bCryptPasswordEncoder.encode(Utilizator.getParola()));
        Utilizator.setEnabled(true);
        Authority UtilizatorAuthority = AuthorityDAO.findByAuthName("ADMIN");
        Utilizator.setUserAuthority(new HashSet<Authority>(Arrays.asList(UtilizatorAuthority)));
        UtilizatorDAO.save(Utilizator);
    }

}