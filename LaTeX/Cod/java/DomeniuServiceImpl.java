package com.iosif.service;

@Service
public class DomeniuServiceImpl implements DomeniuService {


	@Autowired
	private DomeniuDAO domeniuDAO;

	public List<Domeniu> findAllDomenii() {
		return domeniuDAO.findAll();
	}

	public Domeniu updateDomeniu(Domeniu appointment) {
		return domeniuDAO.saveAndFlush(appointment);
	}

	public Domeniu saveDomeniu(Domeniu appointment) {
		return domeniuDAO.save(appointment);
	}

	public void deleteDomeniuById(Short id) {
		domeniuDAO.deleteById(id);
	}
        
        public Domeniu getDomeniu(Short Id){
                return domeniuDAO.getOne(Id);
        }

}





