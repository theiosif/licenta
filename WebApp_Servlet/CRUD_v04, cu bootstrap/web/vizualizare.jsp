<!DOCTYPE html>
<%@page language="java"
        contentType="text/html; pageEncoding=ISO-8859-1" %>
<%@ page import="com.dao.DisciplinaDAO, com.model.*, java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html lang="en" dir="ltr">
  <head>
    <meta charset="ISO-8859-1">
    <title>Vizualizare</title>
    <!--STYLE-->

    <!--includem css-ul din bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- includem google fonts pt icon-uri si font-uri-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <!--includem css-ul creat de noi-->
    <link rel="stylesheet" href="style.css">
  </head>

<nav class="navbar navbar-dark bg-blue navbar-fixed-top">
    <i class="material-icons md-36 md-light ">school</i>
    <!-- Logo Propriu?
    <img src=" " width="30" height="30" class="d-inline-block align-top" alt="">
    -->
    <a class="navbar-brand" href="vizualizare.jsp">Planuri de invatamant actuale</a>
    <form class="form-inline align-items-end">
      <a href="login.jsp" class="btn btn-outline-light" type="submit" role="button">Log in</a>
    </form>
</nav>


<body>



<%
List<Disciplina> list=DisciplinaDAO.getAllRecords();
request.setAttribute("list",list);
%>

<!--Input de search -->
<div class="container search">
  <div class="row">
    <div class="col-md-12">
            <div class="input-group" id="adv-search">

                <div class="input-group-btn" style="position:unset">
                    <div class="btn-group" role="group" style="position:unset">
                      
                        
                        <!--Dropdown-->
                        <div class="dropdown dropdown-lg">

                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret">Filtreaza</span></button>
                            <div class=" search dropdown-menu dropdown-menu-left" role="menu">
                                
                                
                                
                                <form class="form-horizontal" role="form" name="spanac" action="spanac" method="get">
                                    
                                  <div class="form-group">
                                    <label for="spanac">Domeniu</label>
                                    <select name="domeniu" class="form-control">
                                        <option value="0"> </option>
                                        <option value="1">ETTI</option>
                                        <option value="2">CTI</option>
                                    </select>
                                  </div>

                                  <div class="form-group">
                                    <label for="spanac">Specializare</label>
                                    <select class="form-control" name="specializare">
                                      <option value="*"> </option>
                                      <option value="ELA">ELA</option>
                                      <option value="TST">TST</option>
                                      <option value="RST">RST</option>
                                      <option value="MON">MON</option>
                                      <option value="ELAen">ELAen</option>
                                      <option value="TSTen">TSTen</option>
                                    </select>
                                  </div>

                                  <div class="form-group">
                                    <label for="spanac" name="an">An</label>
                                    <select class="form-control">
                                        <option value="*"> </option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                  </div>

                                  <div class="form-group">
                                    <label for="spanac">Semestru</label>
                                    <select class="form-control" name="semestru">
                                      <option value="*"> </option>
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                        <button type="submit" value="spanac" class="btn btn-outline-info">Filtreaza</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
               
                <form  class='row' role="form" name="spanac" action="spanac" method="get">
                    
                  <input type="text" name="keyword"  placeholder="Numele disciplinei..." />
                  <button type="submit" class="btn btn-outline-info">Cauta</button>
                    
                </form>
            
            </div>

          </div>
        </div>
  </div>
</div>


<!-- Tabel -->

<div class="container center">
  <table class ="table table-hover" style="position:absolute" >

    <thead class ="thead-color">
      <tr>
          <th scope="col">Cod</th>
          <th  scope="col">Domeniu</th>
          <th  scope="col">Specializare</th>
          <th  scope="col">Nume Disciplina</th>
          <th  scope="col">An</th>
          <th  scope="col">Semestru</th>
          <th  scope="col">ECTS</th>
          <th  scope="col">Tip Examinare</th>
      </tr>
    </thead>

    <tbody>
      <c:forEach items="${list}" var="u">
              <tr>
                  <th scope="row">${u.getCod1()}${u.getCod2()}${u.getCod3()}${u.getCod4()}${u.getCod5()}</th>
                  <td>${u.getIdDomeniu()}</td>
                  <td>${u.getIdSpecializare()}</td>
                  <td>${u.getNumeDisciplina()}</td>
                  <td>
                      <fmt:formatNumber value="${(u.getCod3()+1)/2}" maxFractionDigits="0" />
                  </td>
                  <td><c:choose>
                          <c:when test="${u.getCod3()%2!=0}">1</c:when>
                          <c:otherwise>2</c:otherwise>
                      </c:choose>
                  </td>
                  <td>${u.getEcts()}</td>
                  <td>${u.getTipExaminare()}</td>
              </tr>
      </c:forEach>
  </tbody>
  </table>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</body>
</html>
