package com.iosif.entity;

import com.iosif.entity.Disciplina;
import com.iosif.entity.Domeniu;
import com.iosif.entity.User2specializare;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(Specializare.class)
public class Specializare_ { 

    public static volatile SingularAttribute<Specializare, Short> idSpecializare;
    public static volatile SingularAttribute<Specializare, Domeniu> idDomeniu;
    public static volatile CollectionAttribute<Specializare, Disciplina> disciplineCollection;
    public static volatile SingularAttribute<Specializare, String> lbPredare;
    public static volatile CollectionAttribute<Specializare, User2specializare> user2specializareCollection;
    public static volatile SingularAttribute<Specializare, String> numeSpecializare;
    public static volatile SingularAttribute<Specializare, String> codSpecializare;
    public static volatile SingularAttribute<Specializare, String> prescurtareS;

}