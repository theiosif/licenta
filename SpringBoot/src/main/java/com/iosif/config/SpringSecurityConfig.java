package com.iosif.config;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
// HTTPS-only
    @Value("${server.port.http}") //Din application.properties
        int httpPort;
    @Value("${server.port}") //Din application.properties
            int httpsPort;

    @Bean
    public WebFilter httpsRedirectFilter() {
        return new WebFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
                URI originalUri = exchange.getRequest().getURI();

                //here set your condition to http->https redirect
                List<String> forwardedValues = exchange.getRequest().getHeaders().get("x-forwarded-proto");
                if (forwardedValues != null && forwardedValues.contains("http")) {
                    try {
                        URI mutatedUri = new URI("https",
                                originalUri.getUserInfo(),
                                originalUri.getHost(),
                                originalUri.getPort(),
                                originalUri.getPath(),
                                originalUri.getQuery(),
                                originalUri.getFragment());
                        ServerHttpResponse response = exchange.getResponse();
                        response.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                        response.getHeaders().setLocation(mutatedUri);
                        return Mono.empty();
                    } catch (URISyntaxException e) {
                        throw new IllegalStateException(e.getMessage(), e);
                    }
                }
                return chain.filter(exchange);
            }
        };
    }

//Specificare SaltedHash pentru stocarea parolelor
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

//query-urile din xxConfig
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") //dont ask
    @Autowired
    private DataSource dataSource;


    @Value("select NUME, PAROLA, ENABLED from utilizatori where NUME=?")
    private String usersQuery;

    @Value("SELECT a.AUTH_NAME, u.NUME from authority a natural join utilizatori u where u.NUME=?")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.
                jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                //.userDetailsService(CustomUserDetailsService())
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //redirect to HTTPS
        http.portMapper()
                .http(httpPort) // http port defined in yml config file
                .mapsTo(httpsPort); // https port defined in yml config file

        //XSS
        http.headers()
                .frameOptions().sameOrigin().xssProtection().block(false).xssProtectionEnabled(true);

        //Mai Mult XSS
        http.headers().contentSecurityPolicy(
  "script-src 'self' https://maxcdn.bootstrapcdn.com https://ajax.googleapis.com https://cdnjs.cloudflare.com;" +
                "object-src 'self' https://fonts.googleapis.com https://maxcdn.bootstrapcdn.com;" +
                "style-src 'self' https://fonts.googleapis.com https://maxcdn.bootstrapcdn.com;" +
                "report-uri /TODO/endpoint-raportare-incident");

        //Session Hijack cu Istoric
        http.headers().cacheControl();

        //Fara giumbuslucuri din alta parte
        http.headers().referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.SAME_ORIGIN);


        http.authorizeRequests()
            .antMatchers("/", "/disciplina/*", "/domspec/*", "/auth/login").permitAll()
        .and()

        .formLogin()
            .loginPage("/")
            .usernameParameter("usrname")
            .passwordParameter("psw")
            .loginProcessingUrl("/auth/login")
            .defaultSuccessUrl("/gestiune/lista")
            .failureUrl("/?eroareLogin=1")
            .permitAll()
        .and()

            .logout()
            /* .addLogoutHandler(new HeaderWriterLogoutHandler(
                    new ClearSiteDataHeaderWriter("*")));*/
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            //.logoutUrl("/logout")
            .logoutSuccessUrl("/")
            .clearAuthentication(true)

            .deleteCookies("JSESSIONID")
            .invalidateHttpSession(true)
            .permitAll()

        .and()
            .exceptionHandling() //exception handling configuration
            .accessDeniedPage("/error")

        .and()
        .rememberMe().rememberMeParameter("remember-me-new").key("uniqueAndSecret").tokenValiditySeconds(86400);

        http.authorizeRequests().antMatchers("/gestiune/*").hasAnyAuthority("admin", "user").anyRequest().authenticated();
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
            .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "*.css", "*.js");
    }

}
