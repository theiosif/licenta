package com.iosif.dao;

import com.iosif.entity.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.iosif.entity.Utilizator;

import java.util.Collection;

@Repository
public interface UtilizatorDAO extends JpaRepository<Utilizator, Integer> {

    Utilizator findByNume(String nume);
    Utilizator findByIdUtilizator(Integer id);
}