INSERT INTO `planuri_invatamant`.`facultati`
	(`COD_FAC`, `NUME_FAC`, `PRESCURTARE_FAC`) VALUES
		(4, "ELECTRONICA, TELECOMUNICATII SI TEHNOLOGIA INFORMATIEI", "ETTI");


INSERT INTO `planuri_invatamant`.`domenii`
	(`ID_FAC`, `COD_DOMENIU`, `PRESCURTARE_D`, `NUME_DOMENIU`) VALUES
		(1, "100", "IETTI",
			"Inginerie electronică, telecomunicații și tehnologii informaționale"),
		(1, "010", "CTI",
			"Calculatoare şi tehnologia informaţiei");


INSERT INTO `planuri_invatamant`.`specializari`
	(`ID_FAC`, `ID_DOMENIU`, `COD_SPECIALIZARE`, `NUME_SPECIALIZARE`, `PRESCURTARE_S`) VALUES
		(1, 1, "010", "Electronică aplicată", "ELA"),
        (1, 1, "020", "Tehnologii şi sisteme de telecomunicaţii", "TST"),
        (1, 1, "030", "Reţele şi software de telecomunicaţii", "RST"),
        (1, 1, "040", "Microelectronică, optoelectronică şi nanotehnologii", "MON"),
        (1, 2, "040", "Ingineria informaţiei", "INF");

INSERT INTO `planuri_invatamant`.`specializari`
	(`ID_FAC`, `ID_DOMENIU`, `COD_SPECIALIZARE`, `NUME_SPECIALIZARE`, `PRESCURTARE_S`, `LB_PREDARE`) VALUES
		(1, 1, "010", "Electronică aplicată", "ELAen", "EN"),
        (1, 1, "020", "Tehnologii şi sisteme de telecomunicaţii", "TSTen", "EN");

INSERT INTO `planuri_invatamant`.`discipline`
	(`COD_1`, `COD_2`, `COD_3`, `COD_4`, `COD_5`,
     `NUME_DISCIPLINA`,
     `ID_FAC`, `ID_DOMENIU`, `ID_SPECIALIZARE`,
	 `ORE_SAPT_C`, `ORE_SAPT_S`,  `ORE_SAPT_L`,  `ORE_SAPT_P`, `ORE_SAPT_PW`,
     `ECTS`, `SAPTAMANI`, `TIP_EXAMINARE`)

		VALUES

		("04", "D", "05", "O", "001",
        "Teoria transmisiunii informaţiei",
         1, 1, 2,
         3, 1, 1, 0, 0,
         5, 14, "E"),


         ("04", "D", "06", "O", "001",
        "Information Transmission Theory",
         1, 1, 3,
         3, 1, 1, 0, 0,
         5, 14, "E"),


		("04", "F", "01", "O", "001", "Analiză matematică 1",
        1, 1, 2,
        3, 1, 0, 0, 0,
        4, 14, "E");

-- date useri
INSERT INTO `planuri_invatamant`.`utilizatori` (TIP_UTILIZATOR, NUME, PAROLA, EMAIL, ENABLED) VALUES
  ('admin', 'admin', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'admin_email@etti.pub.ro', 1),
  ('profesor', 'prof1', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'prof1_email@etti.pub.ro', 1),
  ('profesor', 'prof2', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'prof2_email@etti.pub.ro', 1),
  ('sef-specializare', 'sefRST', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'sef-specializare_email@etti.pub.ro', 1);

INSERT INTO `planuri_invatamant`.`authority` (AUTH_NAME) VALUES ('ADMIN');
INSERT INTO `planuri_invatamant`.`authority` (AUTH_NAME) VALUES ('USER');

INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (1, 1);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (1, 2);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (2, 1);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (3, 1);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (4, 1);