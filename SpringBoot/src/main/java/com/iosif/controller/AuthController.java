package com.iosif.controller;

import java.util.List;

import com.iosif.entity.Specializare;
import com.iosif.entity.Utilizator;
import com.iosif.service.SpecializareService;
import com.iosif.service.UtilizatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.iosif.entity.Disciplina;
import com.iosif.service.DisciplinaService;
import org.springframework.web.bind.annotation.RestController;

import com.iosif.service.DomeniuService;
import com.iosif.entity.Domeniu;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private DomeniuService domeniuService;

    @Autowired
    private SpecializareService specializareService;

    @Autowired
    private DisciplinaService disciplinaService;

    @Autowired
    private UtilizatorService utilizatorService;
    @PostMapping("/login")
    public String procesareLogin(Model theModel) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilizator user = utilizatorService.findUtilizatorByNume(auth.getName());
        theModel.addAttribute("userName", "Nume Utilizator:" + user.getNume() + " (" + user.getEmail() + ")");
        theModel.addAttribute("adminMessage", "(Pentru Debugging)");

        List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
        theModel.addAttribute("discipline", theDiscipline);

        List<Domeniu> theDomenii = domeniuService.findAllDomenii();
        theModel.addAttribute("domenii", theDomenii);

        List<Specializare> theSpecializari = specializareService.findAllSpecializari();
        theModel.addAttribute("specializari", theSpecializari);

        return "domspec/lista";
    }

    /*@PostMapping("/login")
    public ModelAndView procesareLogin(HttpServletRequest req, RedirectAttributes redir){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/dommspec/lista");


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilizator user = utilizatorService.findUtilizatorByNume(auth.getName());

        redir.addFlashAttribute("userName", "Nume Utilizator:" + user.getNume() + " (" + user.getEmail() + ")");
        redir.addFlashAttribute("adminMessage", "(Pentru Debugging)");

        List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
        redir.addFlashAttribute("discipline", theDiscipline);

        List<Domeniu> theDomenii = domeniuService.findAllDomenii();
        redir.addFlashAttribute("domenii", theDomenii);

        List<Specializare> theSpecializari = specializareService.findAllSpecializari();
        redir.addFlashAttribute("specializari", theSpecializari);

        return mav;
    }*/
}
