<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

<!DOCTYPE html>
<html>

<%@include file="head-meta.jsp" %>


<body>
<nav class="navbar navbar-dark nav-admin navbar-fixed-top justify-content-between">

	<div class="container-fluid nav-ul"><ul>
		<!-- Logo Propriu? --> <%--<i class="material-icons md-36 md-light ">school</i>--%>
		<li><div class="navbar-brand" href="#">
			<a href="https://upb.ro/"><img src="resources/media/logo_upb_min.png" class="d-inline-block align-top" alt="upb_logo"></a>
			<a href="http://www.electronica.pub.ro"><img src="resources/media/logo_etti_min.png" class="d-inline-block align-top" alt="etti_logo"></a>
		</div></li>


		<li><a href="" class="navbar-brand navbar-text text-center text-white"><h1>Planuri de invatamant</h1></a></li>
	<li>
    <form class="form-inline align-items-end" action="logout" method="post">
		<input type="hidden" name="${_csrf.parameterName}"
			   value="${_csrf.token}" />
		<button type="submit" class="btn btn-outline-light btn-lg" ><span class="glyphicon glyphicon-off"></span> Logout</button>
    </form>
	</li></ul></div>
</nav>

<br>
<br>


<jsp:include page="debug.jsp"></jsp:include>

<div class="container">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link color-admin" id="domenii-tab" data-toggle="tab" href="#domenii" role="tab" aria-controls="domenii" aria-selected="false">Domenii</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active show color-admin" id="specializari-tab" data-toggle="tab" href="#specializari" role="tab" aria-controls="specializari" aria-selected="false">Specializari</a>
        </li>
		<li class="nav-item">
			<a class="nav-link color-admin" id="discipline-tab" data-toggle="tab" href="#discipline" role="tab" aria-controls="discipline" aria-selected="true">Discipline</a>
		</li>
    </ul>
</div>

<div class="tab-content" id="myTabContent">

<!-- TAB DOMENII -->
<div class="tab-pane fade" id="domenii" role="tabpanel" aria-labelledby="domenii-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />

			<form class="form-inline align-items-end" action="gestiune/domeniu/showForm" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					   value="${_csrf.token}" />
				<button type="submit" class="btn btn-primary bg-admin" ><span class="glyphicon glyphicon-off"></span>Adauga Domeniu</button>
			</form>
				<br/><br/>
			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Domeniilor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th><h5>Cod</h5></th>
							<th><h5>Prescurtare</h5></th>
							<th><h5>Nume</h5></th>
                            <th colspan="2" class="text-center nav-admin text-white"><h4>Operatiuni</h4></th>
						</tr>

						<c:forEach var="tempDomeniu" items="${domenii}">

							<!-- constructie link-uri domnii -->
							<c:url var="updateLink" value="/gestiune/domeniu/updateForm">
								<c:param name="domeniuId" value="${tempDomeniu.idDomeniu}" />
							</c:url>
							<c:url var="deleteLink" value="/gestiune/domeniu/delete">
								<c:param name="domeniuId" value="${tempDomeniu.idDomeniu}" />
							</c:url>

							<tr>
								<td>${tempDomeniu.codDomeniu}</td>

								<td>${tempDomeniu.prescurtareD}</td>

								<td>${tempDomeniu.numeDomeniu}</td>

								<td>
									<form class="form-inline align-items-end" action="${updateLink}" method="post">
										<input type="hidden" name="${_csrf.parameterName}"
											   value="${_csrf.token}" />
										<button type="submit" class="btn btn-sm btn-primary bg-admin" ><span class="glyphicon glyphicon-off"></span>Modifica</button>
									</form>
								</td><td>
								<form class="form-inline align-items-end" action="${deleteLink}" method="post">
									<input type="hidden" name="${_csrf.parameterName}"
										   value="${_csrf.token}" />
									<button type="submit" class="btn btn-sm btn-primary bg-admin confirmare"><span class="glyphicon glyphicon-off"></span>Sterge</button>
								</form>
							</td>

							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>
	</div>
</div> <!-- end div domenii -->

<!-- TAB SPECIALIZARI -->
<div class="tab-pane fade show active" id="specializari" role="tabpanel" aria-labelledby="specializari-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />
			<form class="form-inline align-items-end" action="gestiune/specializare/showForm" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					   value="${_csrf.token}" />
				<button type="submit" class="btn btn-primary bg-admin" ><span class="glyphicon glyphicon-off"></span>Adauga Specializare</button>
			</form>
				<br/><br/>


			<div class="card card-info">
                <div class="card-header">
					<div class="card-title h2">Lista Specializarilor</div>
				</div>

				<div class="card-body">
                    <div class="container">
                        <!-- loop pentru domenii // constructie tab headers -->
                        <ul class="nav nav-tabs" id="TabHeaderDomenii" role="tablist">
                            <c:forEach var="tempDomeniu" items="${domenii}" varStatus = "status">
                            <li ${status.first ? 'class="active"' : ''}>
                                <a data-toggle="tab" class="nav-link color-admin" href="#${tempDomeniu.prescurtareD}">${tempDomeniu.prescurtareD}</a>
                            </li>
                            </c:forEach>
                        </ul>
					 </div>


					<div class="tab-content" id="TabContentDomenii">

					<!-- outer loop pentru domenii // constructie tab content -->
                    <c:forEach var="tempDomeniu" items="${domenii}" varStatus = "status">
                        <div class="tab-pane fade ${status.first ? 'show active' : ''}" id="${tempDomeniu.prescurtareD}">
                            <div class="container">
                                <div class="col-md-offset-1 col-md-10">
                                    <hr />
                                    <table class="table table-striped table-bordered">
        								<tr>
        									<th><h5>Cod</h5></th>
        									<th><h5>Prescurtare</h5></th>
        									<th><h5>Nume</h5></th>
                                            <th colspan="2" class="text-center nav-admin text-white"><h4>Operatiuni</h4></th>
        								</tr>

        								<!-- inner loop pentru specializari -->
        								<c:forEach var="tempSpecializare" items="${tempDomeniu.specializariCollection}">

        									<!-- Constructie Link -->
        									<c:url var="updateLink" value="/gestiune/specializare/updateForm">
        										<c:param name="specializareId" value="${tempSpecializare.idSpecializare}" />
        									</c:url>

											<c:url var="deleteLink" value="/gestiune/specializare/delete">
        										<c:param name="specializareId" value="${tempSpecializare.idSpecializare}" />
        									</c:url>

        									<tr>
        										<td>${tempSpecializare.codSpecializare}</td>
        										<td>${tempSpecializare.prescurtareS}</td>
        										<td>${tempSpecializare.numeSpecializare}</td>
												<td>
													<form class="form-inline align-items-end" action="${updateLink}" method="post">
														<input type="hidden" name="${_csrf.parameterName}"
															   value="${_csrf.token}" />
														<button type="submit" class="btn btn-sm btn-primary bg-admin" ><span class="glyphicon glyphicon-off"></span>Modifica</button>
													</form>
												</td><td>
												<form class="form-inline align-items-end" action="${deleteLink}" method="post">
													<input type="hidden" name="${_csrf.parameterName}"
														   value="${_csrf.token}" />
													<button type="submit" class="btn btn-sm btn-primary bg-admin confirmare"><span class="glyphicon glyphicon-off"></span>Sterge</button>
												</form>
											</td>
        									</tr>

                                        </c:forEach> <!-- endloop dom.specializari -->
                                    </table>
                                </div>
                            </div>
                        </div>
					</c:forEach> <!-- endloop domenii -->
                    </div> <!-- End of Tab Content -->

                </div> <!-- end of card-body -->
            </div> <!-- end of card card-info -->
        </div> <!-- end of col whatever -->
    </div> <!-- end of container -->
</div> <!-- end of tab pane SPECIALIZARI -->


<!-- TAB DISCIPLINE -->
<div class="tab-pane fade" id="discipline" role="tabpanel" aria-labelledby="discipline-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />

			<form class="form-inline align-items-end" action="gestiune/disciplina/showForm" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					   value="${_csrf.token}" />
				<button type="submit" class="btn btn-primary bg-admin" ><span class="glyphicon glyphicon-off"></span>Adauga Disciplina</button>
			</form>

			<br/><br/>
			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Disciplinelor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th class="text-center"><h5>Cod</h5></th>
							<th class="text-center"><h5>Domeniu</h5></th>
							<th class="text-center"><h5>Specializare</h5></th>
							<th class="text-center"><h5>Nume Disciplina</h5></th>
							<th colspan="2" class="text-center nav-admin text-white"><h4>Operatiuni</h4></th>

						</tr>

						<!-- loop over and print our discipline -->
						<c:forEach var="tempDisciplina" items="${discipline}">

							<!-- construct an "update" link with disciplina id -->
							<c:url var="updateLink" value="gestiune/disciplina/updateForm">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<!-- construct an "delete" link with disciplina id -->
							<c:url var="deleteLink" value="gestiune/disciplina/delete">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<tr>
								<td>${tempDisciplina.cod1}${tempDisciplina.cod2}${tempDisciplina.cod3}${tempDisciplina.cod4}${tempDisciplina.cod5}</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idDomeniu.numeDomeniu}">${tempDisciplina.idDomeniu.prescurtareD}</a>
								</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idSpecializare.numeSpecializare}">${tempDisciplina.idSpecializare.prescurtareS}</a>
								</td>

								<td>${tempDisciplina.numeDisciplina}</td>

								<td>
									<form class="form-inline align-items-end" action="${updateLink}" method="post">
										<input type="hidden" name="${_csrf.parameterName}"
											   value="${_csrf.token}" />
										<button type="submit" class="btn btn-sm btn-primary bg-admin" ><span class="glyphicon glyphicon-off"></span>Modifica</button>
									</form>
								</td><td>
								<form class="form-inline align-items-end" action="${deleteLink}" method="post">
									<input type="hidden" name="${_csrf.parameterName}"
										   value="${_csrf.token}" />
									<button type="submit" class="btn btn-sm btn-primary bg-admin confirmare"><span class="glyphicon glyphicon-off"></span>Sterge</button>
								</form>
							</td>

							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>
	</div>
</div> <!-- end div disicpline -->

</div> <!-- end tab content Parent -->

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/tooltip.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/confirmare.js"></script>
</body>
</html>
