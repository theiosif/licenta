<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>[UPB] Lista Disciplinelor</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<!-- includem google fonts pt icon-uri si font-uri-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<%-- CSS PROPRIU-ish --%>
	<link href="<c:url value="/resources/css/style.css" />"	rel="stylesheet">
</head>


<body>

	<nav class="navbar navbar-dark bg-blue navbar-fixed-top">
        <i class="material-icons md-36 md-light ">school</i>
        <!-- Logo Propriu?
        <img src=" " width="30" height="30" class="d-inline-block align-top" alt="">
        -->
        <a class="navbar-brand" href="/planuri_invatamant">Planuri de invățământ</a>
        <form class="form-inline align-items-end">
    			<a href="/planuri_invatamant/login" style="color:inherit">
    				<button type="button" class="btn btn-outline-light">
    					Login
    				</button>
    			</a>
        </form>
    </nav>

<br>
<br>

    <div class="container">
      <div class="card-deck mb-6 text-center">

        <div class="card mb-6 box-shadow">
          <div class="card-header">
            <h3 class="my-0 font-weight-normal">Discipline</h3>
          </div>
          <div class="card-body d-flex flex-column">
            <h4 class="card-title pricing-card-title">Planul curent de învățământ</h4>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Detalii per disciplină</li>
              <li><small class="text-muted">ECTS/ repartizarea orelor/ limbă predare, etc.</small></li>
            </ul>
			<p><a class="btn btn-primary mt-auto align-bottom" href="disciplina/lista" role="button">
				Discipline &raquo;
			</a></p>
          </div>
        </div>

        <div class="card mb-6 box-shadow">
          <div class="card-header">
			<h3 class="my-0 font-weight-normal">Oferte de Studiu</h3>
		  </div>

          <div class="card-body d-flex flex-column">
            <h4 class="card-title pricing-card-title">Domeniile și specializările acestora</h4>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Lista domeniilor/ specializărilor</li>
              <li><small class="text-muted">Descrieri sumare</small></li>
            </ul>
			<p><a class="btn btn-primary mt-auto align-bottom" href="domspec/lista" role="button">
				Domenii & Specializări &raquo;
			</a></p>
          </div>
        </div>

      </div>

</body>

</html>
