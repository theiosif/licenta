package com.iosif.entity;

@Entity
@Audited
@Table(name = "specializari")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Specializare.findAll", query = "SELECT s FROM Specializare s"),
    @NamedQuery(name = "Specializare.findByIdSpecializare", query = "SELECT s FROM Specializare s WHERE s.idSpecializare = :idSpecializare"),
    @NamedQuery(name = "Specializare.findByCodSpecializare", query = "SELECT s FROM Specializare s WHERE s.codSpecializare = :codSpecializare"),
    @NamedQuery(name = "Specializare.findByNumeSpecializare", query = "SELECT s FROM Specializare s WHERE s.numeSpecializare = :numeSpecializare"),
    @NamedQuery(name = "Specializare.findByPrescurtareS", query = "SELECT s FROM Specializare s WHERE s.prescurtareS = :prescurtareS"),
    @NamedQuery(name = "Specializare.findByLbPredare", query = "SELECT s FROM Specializare s WHERE s.lbPredare = :lbPredare")})
public class Specializare implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SPECIALIZARE")
    private Short idSpecializare;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "COD_SPECIALIZARE")
    private String codSpecializare;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "NUME_SPECIALIZARE")
    private String numeSpecializare;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "PRESCURTARE_S")
    private String prescurtareS;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "LB_PREDARE")
    private String lbPredare;
    @JoinColumn(name = "ID_FAC", referencedColumnName = "ID_FAC")
    @ManyToOne(optional = false)
    private Facultate idFac;
    @JoinColumn(name = "ID_DOMENIU", referencedColumnName = "ID_DOMENIU")
    @ManyToOne(optional = false)
    private Domeniu idDomeniu;
    @OneToMany(mappedBy = "idSpecializare")
    private Collection<User2specializare> user2specializareCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSpecializare")
    private Collection<Disciplina> disciplineCollection;

    public Specializare() {
    }

    public Specializare(Short idSpecializare) {
        this.idSpecializare = idSpecializare;
    }

    public Specializare(Short idSpecializare, String codSpecializare, String numeSpecializare, String prescurtareS, String lbPredare) {
        this.idSpecializare = idSpecializare;
        this.codSpecializare = codSpecializare;
        this.numeSpecializare = numeSpecializare;
        this.prescurtareS = prescurtareS;
        this.lbPredare = lbPredare;
    }

    public Short getIdSpecializare() {
        return idSpecializare;
    }

    public void setIdSpecializare(Short idSpecializare) {
        this.idSpecializare = idSpecializare;
    }

    public String getCodSpecializare() {
        return codSpecializare;
    }

    public void setCodSpecializare(String codSpecializare) {
        this.codSpecializare = codSpecializare;
    }

    public String getNumeSpecializare() {
        return numeSpecializare;
    }

    public void setNumeSpecializare(String numeSpecializare) {
        this.numeSpecializare = numeSpecializare;
    }

    public String getPrescurtareS() {
        return prescurtareS;
    }

    public void setPrescurtareS(String prescurtareS) {
        this.prescurtareS = prescurtareS;
    }

    public String getLbPredare() {
        return lbPredare;
    }

    public void setLbPredare(String lbPredare) {
        this.lbPredare = lbPredare;
    }

    public Facultate getIdFac() { return idFac; }

    public void setIdFac(Facultate idFac) {
        this.idFac = idFac;
    }

    public Domeniu getIdDomeniu() {
        return idDomeniu;
    }

    public void setIdDomeniu(Domeniu idDomeniu) {
        this.idDomeniu = idDomeniu;
    }

    @XmlTransient
    public Collection<User2specializare> getUser2specializareCollection() {
        return user2specializareCollection;
    }

    public void setUser2specializareCollection(Collection<User2specializare> user2specializareCollection) {
        this.user2specializareCollection = user2specializareCollection;
    }

    @XmlTransient
    public Collection<Disciplina> getDisciplineCollection() {
        return disciplineCollection;
    }

    public void setDisciplineCollection(Collection<Disciplina> disciplineCollection) {
        this.disciplineCollection = disciplineCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSpecializare != null ? idSpecializare.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Specializare)) {
            return false;
        }
        Specializare other = (Specializare) object;
        if ((this.idSpecializare == null && other.idSpecializare != null) || (this.idSpecializare != null && !this.idSpecializare.equals(other.idSpecializare))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.Specializare[ idSpecializare=" + idSpecializare + " ]";
    }
    
}
