package com.iosif.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.iosif.entity.Domeniu;
import com.iosif.entity.Specializare;


/**
 *
 * @author yoji
 */


// clasa agrega 2 tipuri de entitati.
// nu este neaparat ideal dpdv al encapsularii si decuplarii
// insa varianta a fost realizata mai mult ca si experiment
public interface DomspecDAO {
     
        // aferent domeniilor
   	public List<Domeniu> getDomenii();
	public void saveDomeniu(Domeniu theDomeniu);
	public Domeniu getDomeniu(int theId);
	public void deleteDomeniu(int theId);
        
        
        //aferent specializarilor
        public List<Specializare> getSpecializari();
	public void saveSpecializare(Specializare theSpecializare);
	public Specializare getSpecializare(int theId);
	public void deleteSpecializare(int theId);
}