<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>[UPB] Lista Disciplinelor</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<!-- includem google fonts pt icon-uri si font-uri-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<%-- CSS PROPRIU-ish --%>
	<link href="<c:url value="/resources/css/style.css" />"	rel="stylesheet">
</head>

<script>
$(document).ready(function () {
  $("a").tooltip({
    'selector': '',
    'placement': 'top',
    'container':'body'
  });
});

$('.auto-tooltip').tooltip();
</script>

<nav class="navbar navbar-dark bg-blue navbar-fixed-top">
    <i class="material-icons md-36 md-light ">school</i>
    <!-- Logo Propriu?
    <img src=" " width="30" height="30" class="d-inline-block align-top" alt="">
    -->
    <a class="navbar-brand" href="/planuri_invatamant">Planuri de invatamant</a>
    <form class="form-inline align-items-end">
			<a href="/planuri_invatamant/login" style="color:inherit">
				<button type="button" class="btn btn-outline-light">
					Login
				</button>
			</a>
    </form>
</nav>


<body>
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />

			<input type="button" value="Adaugă Disciplină"
				onclick="window.location.href='showForm'; return false;"
				class="btn btn-primary" />
				<br/><br/>
			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Disciplinelor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th>Cod Disciplină</th>
							<th>Domeniu</th>
							<th>Specializare</th>
							<th>Nume Disciplină</th>

						</tr>

						<!-- loop over and print our discipline -->
						<c:forEach var="tempDisciplina" items="${discipline}">

							<!-- construct an "update" link with disciplina id -->
							<c:url var="updateLink" value="/disciplina/updateForm">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<!-- construct an "delete" link with disciplina id -->
							<c:url var="deleteLink" value="/disciplina/delete">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<tr>
								<td>${tempDisciplina.cod1}${tempDisciplina.cod2}${tempDisciplina.cod3}${tempDisciplina.cod4}${tempDisciplina.cod5}</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idDomeniu.numeDomeniu}">${tempDisciplina.idDomeniu.prescurtareD}</a>
							 	</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idSpecializare.numeSpecializare}">${tempDisciplina.idSpecializare.prescurtareS}</a>
								</td>

								<td>${tempDisciplina.numeDisciplina}</td>

								<td>
									<!-- display the update link --> <a href="${updateLink}">Modifică</a>
									| <a href="${deleteLink}"
									onclick="if (!(confirm('Confirmați ștergerea?'))) return false">Șterge</a>
								</td>

							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>

	</div>
</body>

</html>
