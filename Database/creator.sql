DROP DATABASE IF EXISTS `planuri_invatamant`;

CREATE DATABASE `planuri_invatamant`;
USE `planuri_invatamant`;

--# Pt coduri & whatnot
SET SQL_MODE=PIPES_AS_CONCAT;


--#------------------- utilizatori -------------------#
CREATE TABLE `utilizatori`(
	`ID_UTILIZATOR` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`TIP_UTILIZATOR` ENUM('admin', 'profesor', 'secretara', 'sef-domeniu', 'sef-specializare', 'decan') NOT NULL,
	`NUME` VARCHAR(30) NOT NULL UNIQUE,
	`PAROLA` VARCHAR (30) NOT NULL
);


INSERT INTO `utilizatori` (`TIP_UTILIZATOR`, `NUME`, `PAROLA`)
	VALUES
    ("admin", "admin", "dev"),
    ("secretara", "sec1", "dev"),
    ("decan", "decan", "dev"),
    ("sef-domeniu", "sefdom1", "dev"),
    ("sef-domeniu", "sefdom2", "dev"),
    ("sef-specializare", "sefspec1", "dev"),
    ("sef-specializare", "sefspec2", "dev");
------------------ login-related (v01 - cookie UID) ------------------#
CREATE TABLE `sesiuni`(
	`ID_SESIUNE` VARCHAR(21) PRIMARY KEY NOT NULL ,
    `ID_UTILIZATOR` INT UNSIGNED,
    `CREATED` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT FOREIGN KEY (`ID_UTILIZATOR`) REFERENCES `utilizatori`(`ID_UTILIZATOR`) ON DELETE CASCADE ON UPDATE CASCADE
);



--#------------------ facultati ------------------#
--# COD_1 [CHAR(2)] reprezinta ID_FAC (ETTI are ID_FAC=4)
--# tabel creat pt scalare pt intregul UPB.

CREATE TABLE `facultati`(
	`ID_FAC` TINYINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `COD_FAC` TINYINT UNSIGNED NOT NULL,
	`NUME_FAC` VARCHAR(70) NOT NULL,
	`PRESCURTARE_FAC` VARCHAR (10) NOT NULL
);

INSERT INTO `facultati`
	(`COD_FAC`, `NUME_FAC`, `PRESCURTARE_FAC`) VALUES
		(4, "ELECTRONICA, TELECOMUNICATII SI TEHNOLOGIA INFORMATIEI", "ETTI");

--# pt afisat cu zerouri in fata, folosim LPAD:
--# SELECT LPAD(COD_FAC, 2, '0') FROM facultati;


--#------------------- domenii -------------------#

CREATE TABLE `domenii`(
	`ID_DOMENIU` TINYINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `COD_DOMENIU` ENUM('100', '010') NOT NULL,
	`NUME_DOMENIU` VARCHAR(70) NOT NULL,
	`PRESCURTARE_D` VARCHAR (5) NOT NULL
);

INSERT INTO `domenii`
	(`COD_DOMENIU`, `PRESCURTARE_D`, `NUME_DOMENIU`) VALUES
		("100", "IETTI",
			"Inginerie electronică, telecomunicații și tehnologii informaționale"),
		("010", "CTI",
			"Calculatoare şi tehnologia informaţiei");




--#----------------- specializari -----------------#

CREATE TABLE `specializari`(
	`ID_SPECIALIZARE` TINYINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `ID_DOMENIU` TINYINT UNSIGNED NOT NULL,
    `COD_SPECIALIZARE` ENUM('010','020','030','040') NOT NULL,
    `NUME_SPECIALIZARE` VARCHAR(60) NOT NULL,
    `PRESCURTARE_S` ENUM ('ELA', 'TST', 'RST', 'MON', 'INF', 'ELAen', 'TSTen') NOT NULL,
    `LB_PREDARE` ENUM ('RO', 'EN') NOT NULL DEFAULT "RO",
    CONSTRAINT FOREIGN KEY (`ID_DOMENIU`) REFERENCES `domenii`(`ID_DOMENIU`) ON DELETE CASCADE ON UPDATE CASCADE
    );

INSERT INTO `specializari`
	(`ID_DOMENIU`, `COD_SPECIALIZARE`, `NUME_SPECIALIZARE`, `PRESCURTARE_S`) VALUES
		(1, "010", "Electronică aplicată", "ELA"),
        (1, "020", "Tehnologii şi sisteme de telecomunicaţii", "TST"),
        (1, "030", "Reţele şi software de telecomunicaţii", "RST"),
        (1, "040", "Microelectronică, optoelectronică şi nanotehnologii", "MON"),
        (2, "040", "Ingineria informaţiei", "INF");

INSERT INTO `specializari`
	(`ID_DOMENIU`, `COD_SPECIALIZARE`, `NUME_SPECIALIZARE`, `PRESCURTARE_S`, `LB_PREDARE`) VALUES
		(1, "010", "Electronică aplicată", "ELAen", "EN"),
        (1, "020", "Tehnologii şi sisteme de telecomunicaţii", "TSTen", "EN");


--# UPDATE `discipline` SET `LB_PREDARE`="EN" WHERE `ID_DISCIPLINA`=2;   /// ?!?



--#----------------- discipline -----------------#

CREATE TABLE `discipline`(
	`ID_DISCIPLINA`  INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `ID_DOMENIU`  TINYINT UNSIGNED NOT NULL,
    `ID_SPECIALIZARE`  TINYINT UNSIGNED NOT NULL,
    `COD_1` CHAR(2) NOT NULL,
    `COD_2` CHAR(1) NOT NULL,
    `COD_3` CHAR(2) NOT NULL,   --#Joaca rol de semestru (si implicit an)
    `COD_4` CHAR(1) NOT NULL,
    `COD_5` CHAR(3) NOT NULL,
    `NUME_DISCIPLINA` VARCHAR(40) NOT NULL,
    `LB_PREDARE` ENUM('RO','EN') NOT NULL DEFAULT "RO",
    `ECTS` TINYINT UNSIGNED NOT NULL,
    `ORE_SAPT_C` TINYINT UNSIGNED NOT NULL,
    `ORE_SAPT_S` TINYINT UNSIGNED NOT NULL,
    `ORE_SAPT_L` TINYINT UNSIGNED NOT NULL,
    `ORE_SAPT_P` TINYINT UNSIGNED NOT NULL,
    `ORE_SAPT_PW` TINYINT UNSIGNED NOT NULL,
    `SAPTAMANI` INT DEFAULT 14,
    `TIP_EXAMINARE` ENUM('E','A/R', 'V'),
    CONSTRAINT FOREIGN KEY (`ID_DOMENIU`) REFERENCES `domenii`(`ID_DOMENIU`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FOREIGN KEY (`ID_SPECIALIZARE`) REFERENCES `specializari`(`ID_SPECIALIZARE`) ON DELETE CASCADE ON UPDATE CASCADE
    );


--#------------------- cine editeaza ce anume -------------------#

CREATE TABLE `user2disciplina`(
	`ID_DREPT` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`ID_UTILIZATOR` INT UNSIGNED,
	`ID_DISCIPLINA` INT UNSIGNED,
	CONSTRAINT FOREIGN KEY (`ID_UTILIZATOR`) REFERENCES `utilizatori`(`ID_UTILIZATOR`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FOREIGN KEY (`ID_DISCIPLINA`) REFERENCES `discipline`(`ID_DISCIPLINA`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `user2specializare`(
	`ID_DREPT` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`ID_UTILIZATOR` INT UNSIGNED,
	`ID_SPECIALIZARE` TINYINT UNSIGNED,
	CONSTRAINT FOREIGN KEY (`ID_UTILIZATOR`) REFERENCES `utilizatori`(`ID_UTILIZATOR`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FOREIGN KEY (`ID_SPECIALIZARE`) REFERENCES `specializari`(`ID_SPECIALIZARE`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `user2domeniu`(
	`ID_DREPT` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`ID_UTILIZATOR` INT UNSIGNED,
	`ID_DOMENIU` TINYINT UNSIGNED,
	CONSTRAINT FOREIGN KEY (`ID_UTILIZATOR`) REFERENCES `utilizatori`(`ID_UTILIZATOR`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FOREIGN KEY (`ID_DOMENIU`) REFERENCES `domenii`(`ID_DOMENIU`) ON DELETE CASCADE ON UPDATE CASCADE
);

--# @TODO Scheduled Task in mysql:
--# Every 2h clean sessions older than 2h.



--# @TODO: parser python pentru csv-ul disciplinelor reale.


--# dummy driver values, pentru testare
INSERT INTO `discipline`
	(`COD_1`, `COD_2`, `COD_3`, `COD_4`, `COD_5`,
     `NUME_DISCIPLINA`,
     `ID_DOMENIU`, `ID_SPECIALIZARE`,
	 `ORE_SAPT_C`, `ORE_SAPT_S`,  `ORE_SAPT_L`,  `ORE_SAPT_P`, `ORE_SAPT_PW`,
     `ECTS`, `SAPTAMANI`, `TIP_EXAMINARE`)

		VALUES

		("04", "D", "05", "O", "001",
        "Teoria transmisiunii informaţiei",
         1, 2,
         3, 1, 1, 0, 0,
         5, 14, "E"),


         ("04", "D", "06", "O", "001",
        "Information Transmission Theory",
         1, 3,
         3, 1, 1, 0, 0,
         5, 14, "E"),


		("04", "F", "01", "O", "001", "Analiză matematică 1",
        1, 2,
        3, 1, 0, 0, 0,
        4, 14, "E");

--### REDUNDANTA:
--# ORE_TOTALE_x = NR_SAPT * ORE_SAPT_x
--# STUDIU_INDIV = 26 * ECTS - ORE_TOTALE4,
--# COD_1 este codul facultatii --> extra tabel
--# COD_3 contine info legata de semestru/an
