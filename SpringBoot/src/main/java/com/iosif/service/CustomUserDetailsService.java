package com.iosif.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.iosif.dao.UtilizatorDAO;
import com.iosif.entity.Authority;
import com.iosif.entity.Utilizator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UtilizatorDAO UtilizatorDAO;
    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {
        Utilizator utilizatorActiv = UtilizatorDAO.findByNume(userName);
        
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

        //WORKAROUND IMENS
        Collection<Authority> authCollection= utilizatorActiv.getUserAuthority();
        String autorizari = authCollection.stream()
                .map(Authority::getAuthName)
                .collect(Collectors.joining(", "));

        String[] authStrings = autorizari.split(", ");
        for(String authString : authStrings) {
            authorities.add(new SimpleGrantedAuthority(authString));
        }

        UserDetails userDetails = (UserDetails)new User(utilizatorActiv.getNume(),
                utilizatorActiv.getParola(), authorities);

        return userDetails;
    }
}