package com.iosif.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iosif.dao.DisciplinaDAO;
import com.iosif.entity.Disciplina;

@Service
public class DisciplinaServiceImpl implements DisciplinaService {


	@Autowired
	private DisciplinaDAO disciplinaDAO;

	public List<Disciplina> findAllDiscipline() {
		return disciplinaDAO.findAll();
	}

	public Disciplina updateDisciplina(Disciplina appointment) {
		return disciplinaDAO.saveAndFlush(appointment);
	}

	public Disciplina saveDisciplina(Disciplina appointment) {
		return disciplinaDAO.save(appointment);
	}

	public void deleteDisciplinaById(Integer id) {
		disciplinaDAO.deleteById(id);
	}

	public Disciplina getDisciplina(Integer Id){
                return disciplinaDAO.getOne(Id);
        }
}




