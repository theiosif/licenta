package com.iosif.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.iosif.entity.Disciplina;


/**
 *
 * @author yoji
 */

public interface DisciplinaDAO {
     
   	public List<Disciplina> getDiscipline();

	public void saveDisciplina(Disciplina theDisciplina);

	public Disciplina getDisciplina(int theId);

	public void deleteDisciplina(int theId);
}