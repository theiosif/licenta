/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iosif.controller;

/**
 *
 * @author yoji
 */
import java.util.List;

import com.iosif.entity.Utilizator;
import com.iosif.service.UtilizatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.iosif.entity.Domeniu;
import com.iosif.entity.Specializare;
import com.iosif.service.DomeniuService;
import com.iosif.service.SpecializareService;

import com.iosif.entity.Disciplina;
import com.iosif.service.DisciplinaService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/domspec")
public class DomSpecController {
	@Autowired
	private DomeniuService domeniuService;

	@Autowired
	private SpecializareService specializareService;

	@Autowired
	private DisciplinaService disciplinaService;

	@Autowired
	private UtilizatorService utilizatorService;

	@GetMapping("/lista")
	public String listaDiscipline(Model theModel) {

		List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
		theModel.addAttribute("discipline", theDiscipline);

		List<Domeniu> theDomenii = domeniuService.findAllDomenii();
		theModel.addAttribute("domenii", theDomenii);

		List<Specializare> theSpecializari = specializareService.findAllSpecializari();
		theModel.addAttribute("specializari", theSpecializari);

		return "lista-domspec";
	}
	

}
