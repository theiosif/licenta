<%--
    Document   : index
    Created on : Dec 5, 2018, 1:26:36 AM
    Author     : yoji
--%>
<!DOCTYPE html>
<%@page language="java"
        contentType="text/html"
        pageEncoding="ISO-8859-1" %>

<%@page import="com.dao.DisciplinaDAO, com.model.*, java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!--  <title>Planuri de invatamant - main</title>  -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- includem google fonts pt icon-uri si font-uri-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
        <!--includem css-ul creat de noi-->
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

      <nav class="navbar navbar-dark bg-blue navbar-fixed-top">
          <i class="material-icons md-36 md-light ">school</i>
          <!-- Logo Propriu?
          <img src=" " width="30" height="30" class="d-inline-block align-top" alt="">
          -->
          <a class="navbar-brand" href="#">Planuri de invatamant actuale</a>
          <form class="form-inline align-items-end">
            <a href="login.jsp" class="btn btn-outline-light" type="submit" role="button">Log in</a>
          </form>
      </nav>

        <br/><br/>
        <form class="form-inline align-items-end">
          <div class="center">
            <a href="vizualizare.jsp" class="btn btn-primary" type="submit" role="button">Vizualizare Planuri</a>
          </div>
        </form>
    </body>

</html>
