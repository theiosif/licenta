<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="head-meta.jsp"></jsp:include>

<!DOCTYPE html>
<html>
<body>
<jsp:include page="header.jsp"></jsp:include>

	<div class="container">
		<div class="col-md-offset-1 col-md-10">

            <br/><br/>
			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Disciplinelor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th>Cod Disciplina</th>
							<th>Domeniu</th>
							<th>Specializare</th>
                            <th>An</th>
                            <th>Semestru</th>
							<th>Nume Disciplina</th>
                            <th>ECTS</th>
                            <th>Metoda de Examinare</th>
						</tr>

						<!-- loop over and print our discipline -->
						<c:forEach var="tempDisciplina" items="${discipline}">

							<!-- construct an "update" link with disciplina id -->
							<c:url var="updateLink" value="/disciplina/updateForm">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<!-- construct an "delete" link with disciplina id -->
							<c:url var="deleteLink" value="/disciplina/delete">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<tr>
								<td>${tempDisciplina.cod1}${tempDisciplina.cod2}${tempDisciplina.cod3}${tempDisciplina.cod4}${tempDisciplina.cod5}</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idDomeniu.numeDomeniu}">${tempDisciplina.idDomeniu.prescurtareD}</a>
							 	</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idSpecializare.numeSpecializare}">${tempDisciplina.idSpecializare.prescurtareS}</a>
								</td>

                                <c:set var="an" value="${tempDisciplina.cod3/2}"/>
                                <td><fmt:formatNumber value="${an+(1-(an%1))%1}" type="number" pattern="#"/></td>


                                <c:set var="sem" value="${tempDisciplina.cod3 eq 01 ? 1 : tempDisciplina.cod3 % 2 + 1}"/>
                                <td>${sem}</td>

                                <td>${tempDisciplina.numeDisciplina}</td>

                                <td>${tempDisciplina.ects}</td>

                                <td>${tempDisciplina.tipExaminare}</td>


							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>

	</div>


<!-- Modal Login --><!-- Modal Login -->
<jsp:include page="modal-login.jsp"></jsp:include>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/tooltip.js"></script>
<jsp:include page="head-meta.jsp"></jsp:include>

</body>

</html>
