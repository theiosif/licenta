/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yoji
 */
@Entity
@Table(name = "utilizatori")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Utilizatori.findAll", query = "SELECT u FROM Utilizatori u")
    , @NamedQuery(name = "Utilizatori.findByIdUtilizator", query = "SELECT u FROM Utilizatori u WHERE u.idUtilizator = :idUtilizator")
    , @NamedQuery(name = "Utilizatori.findByTipUtilizator", query = "SELECT u FROM Utilizatori u WHERE u.tipUtilizator = :tipUtilizator")
    , @NamedQuery(name = "Utilizatori.findByNume", query = "SELECT u FROM Utilizatori u WHERE u.nume = :nume")
    , @NamedQuery(name = "Utilizatori.findByParola", query = "SELECT u FROM Utilizatori u WHERE u.parola = :parola")})
public class Utilizatori implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_UTILIZATOR")
    private Integer idUtilizator;
    @Basic(optional = false)
    @Column(name = "TIP_UTILIZATOR")
    private String tipUtilizator;
    @Basic(optional = false)
    @Column(name = "NUME")
    private String nume;
    @Basic(optional = false)
    @Column(name = "PAROLA")
    private String parola;

    public Utilizatori() {
    }

    public Utilizatori(Integer idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public Utilizatori(Integer idUtilizator, String tipUtilizator, String nume, String parola) {
        this.idUtilizator = idUtilizator;
        this.tipUtilizator = tipUtilizator;
        this.nume = nume;
        this.parola = parola;
    }

    public Integer getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(Integer idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public String getTipUtilizator() {
        return tipUtilizator;
    }

    public void setTipUtilizator(String tipUtilizator) {
        this.tipUtilizator = tipUtilizator;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUtilizator != null ? idUtilizator.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilizatori)) {
            return false;
        }
        Utilizatori other = (Utilizatori) object;
        if ((this.idUtilizator == null && other.idUtilizator != null) || (this.idUtilizator != null && !this.idUtilizator.equals(other.idUtilizator))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.model.Utilizatori[ idUtilizator=" + idUtilizator + " ]";
    }
    
}
