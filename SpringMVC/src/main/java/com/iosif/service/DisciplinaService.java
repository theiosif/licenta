package com.iosif.service;

import java.util.List;

import com.iosif.entity.Disciplina;

public interface DisciplinaService {

	public List<Disciplina> getDiscipline();

	public void saveDisciplina(Disciplina theDisciplina);

	public Disciplina getDisciplina(int theId);

	public void deleteDisciplina(int theId);
	
}
