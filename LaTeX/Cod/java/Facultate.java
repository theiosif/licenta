package com.iosif.entity;

@Entity
@Audited
@Table(name = "facultati")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facultate.findAll", query = "SELECT f FROM Facultate f"),
    @NamedQuery(name = "Facultate.findByIdFac", query = "SELECT f FROM Facultate f WHERE f.idFac = :idFac"),
    @NamedQuery(name = "Facultate.findByCodFac", query = "SELECT f FROM Facultate f WHERE f.codFac = :codFac"),
    @NamedQuery(name = "Facultate.findByNumeFac", query = "SELECT f FROM Facultate f WHERE f.numeFac = :numeFac"),
    @NamedQuery(name = "Facultate.findByPrescurtareFac", query = "SELECT f FROM Facultate f WHERE f.prescurtareFac = :prescurtareFac")})
public class Facultate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_FAC")
    private Short idFac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COD_FAC")
    private short codFac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "NUME_FAC")
    private String numeFac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "PRESCURTARE_FAC")
    private String prescurtareFac;

    public Facultate() {
    }

    public Facultate(Short idFac) {
        this.idFac = idFac;
    }

    public Facultate(Short idFac, short codFac, String numeFac, String prescurtareFac) {
        this.idFac = idFac;
        this.codFac = codFac;
        this.numeFac = numeFac;
        this.prescurtareFac = prescurtareFac;
    }

    public Short getIdFac() {
        return idFac;
    }

    public void setIdFac(Short idFac) {
        this.idFac = idFac;
    }

    public short getCodFac() {
        return codFac;
    }

    public void setCodFac(short codFac) {
        this.codFac = codFac;
    }

    public String getNumeFac() {
        return numeFac;
    }

    public void setNumeFac(String numeFac) {
        this.numeFac = numeFac;
    }

    public String getPrescurtareFac() {
        return prescurtareFac;
    }

    public void setPrescurtareFac(String prescurtareFac) {
        this.prescurtareFac = prescurtareFac;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFac != null ? idFac.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facultate)) {
            return false;
        }
        Facultate other = (Facultate) object;
        if ((this.idFac == null && other.idFac != null) || (this.idFac != null && !this.idFac.equals(other.idFac))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.Facultate[ idFac=" + idFac + " ]";
    }
    
}
