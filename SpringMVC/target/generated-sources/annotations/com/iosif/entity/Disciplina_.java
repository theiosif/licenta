package com.iosif.entity;

import com.iosif.entity.Domeniu;
import com.iosif.entity.Specializare;
import com.iosif.entity.User2disciplina;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(Disciplina.class)
public class Disciplina_ { 

    public static volatile SingularAttribute<Disciplina, Specializare> idSpecializare;
    public static volatile SingularAttribute<Disciplina, Integer> idDisciplina;
    public static volatile CollectionAttribute<Disciplina, User2disciplina> user2disciplinaCollection;
    public static volatile SingularAttribute<Disciplina, Short> oreSaptC;
    public static volatile SingularAttribute<Disciplina, Short> oreSaptPw;
    public static volatile SingularAttribute<Disciplina, String> numeDisciplina;
    public static volatile SingularAttribute<Disciplina, String> tipExaminare;
    public static volatile SingularAttribute<Disciplina, Character> cod4;
    public static volatile SingularAttribute<Disciplina, Short> ects;
    public static volatile SingularAttribute<Disciplina, String> cod5;
    public static volatile SingularAttribute<Disciplina, Character> cod2;
    public static volatile SingularAttribute<Disciplina, Domeniu> idDomeniu;
    public static volatile SingularAttribute<Disciplina, String> cod3;
    public static volatile SingularAttribute<Disciplina, String> lbPredare;
    public static volatile SingularAttribute<Disciplina, String> cod1;
    public static volatile SingularAttribute<Disciplina, Short> oreSaptL;
    public static volatile SingularAttribute<Disciplina, Integer> saptamani;
    public static volatile SingularAttribute<Disciplina, Short> oreSaptP;
    public static volatile SingularAttribute<Disciplina, Short> oreSaptS;

}