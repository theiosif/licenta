package com.iosif.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iosif.dao.DisciplinaDAO;
import com.iosif.entity.Disciplina;

@Service
public class DisciplinaServiceImpl implements DisciplinaService {

	@Autowired
	private DisciplinaDAO disciplinaDAO;
	
	@Override
	@Transactional
	public List<Disciplina> getDiscipline() {
		return disciplinaDAO.getDiscipline();
	}

	@Override
	@Transactional
	public void saveDisciplina(Disciplina theDisciplina) {
		disciplinaDAO.saveDisciplina(theDisciplina);
	}

	@Override
	@Transactional
	public Disciplina getDisciplina(int theId) {
		return disciplinaDAO.getDisciplina(theId);
	}

	@Override
	@Transactional
	public void deleteDisciplina(int theId) {
		disciplinaDAO.deleteDisciplina(theId);
	}
}





