<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>[UPB] Lista Disciplinelor</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<!-- includem google fonts pt icon-uri si font-uri-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- CSS PROPRIU-ish  XX ATENTIE RENAME -->
	<link href="<c:url value="/resources/css/style.css" />"	rel="stylesheet">
</head>

<script>
$(document).ready(function () {
  $("a").tooltip({
    'selector': '',
    'placement': 'top',
    'container':'body'
  });
});

$('.auto-tooltip').tooltip();
</script>


<body>
<nav class="navbar navbar-dark navbar-fixed-top nav-admin">
    <i class="material-icons md-36 md-light ">school</i>
    <!-- Logo Propriu?
    <img src=" " width="30" height="30" class="d-inline-block align-top" alt="">
    -->
    <a class="navbar-brand text-center" href="/planuri_invatamant">ADMINISTRARE<br> Planuri de invatamant</a>
    <form class="form-inline align-items-end">
			<a href="/planuri_invatamant/logout" style="color:inherit">
				<button type="button" class="btn btn-outline-light">
					Logout
				</button>
			</a>
    </form>
</nav>


<div class="container">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active color-admin" id="discipline-tab" data-toggle="tab" href="#discipline" role="tab" aria-controls="discipline" aria-selected="true">Discipline</a>
        </li>
        <li class="nav-item">
            <a class="nav-link color-admin" id="domenii-tab" data-toggle="tab" href="#domenii" role="tab" aria-controls="domenii" aria-selected="false">Domenii</a>
        </li>
        <li class="nav-item">
            <a class="nav-link color-admin" id="specializari-tab" data-toggle="tab" href="#specializari" role="tab" aria-controls="specializari" aria-selected="false">Specializări</a>
        </li>
    </ul>
</div>



<div class="tab-content" id="myTabContent">

<!-- TAB DISCIPLINE -->
<div class="tab-pane fade show active" id="discipline" role="tabpanel" aria-labelledby="discipline-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />

			<input type="button" value="Adaugă Disciplină"
				onclick="window.location.href='showForm'; return false;"
				class="btn btn-primary bg-admin" />
				<br/><br/>
			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Disciplinelor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th>Cod Disciplină</th>
							<th>Domeniu</th>
							<th>Specializare</th>
							<th>Nume Disciplină</th>

						</tr>

						<!-- loop over and print our discipline -->
						<c:forEach var="tempDisciplina" items="${discipline}">

							<!-- construct an "update" link with disciplina id -->
							<c:url var="updateLink" value="/disciplina/updateForm">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<!-- construct an "delete" link with disciplina id -->
							<c:url var="deleteLink" value="/disciplina/delete">
								<c:param name="disciplinaId" value="${tempDisciplina.idDisciplina}" />
							</c:url>

							<tr>
								<td>${tempDisciplina.cod1}${tempDisciplina.cod2}${tempDisciplina.cod3}${tempDisciplina.cod4}${tempDisciplina.cod5}</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idDomeniu.numeDomeniu}">${tempDisciplina.idDomeniu.prescurtareD}</a>
							 	</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idSpecializare.numeSpecializare}">${tempDisciplina.idSpecializare.prescurtareS}</a>
								</td>

								<td>${tempDisciplina.numeDisciplina}</td>

								<td>
									<!-- display the update link --> <a href="${updateLink}">Modifică</a>
									| <a href="${deleteLink}"
									onclick="if (!(confirm('Confirmați ștergerea?'))) return false">Șterge</a>
								</td>

						</tr>

					</c:forEach>

				</table>

				</div>
			</div>
		</div>
	</div>
</div> <!-- end div disicpline -->


<!-- TAB DOMENII -->
<div class="tab-pane fade" id="domenii" role="tabpanel" aria-labelledby="domenii-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />

			<input type="button" value="Adaugă Domeniu"
				onclick="window.location.href='showForm'; return false;"
				class="btn btn-primary bg-admin" />
				<br/><br/>
			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Domeniilor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th>Cod Domeniu</th>
							<th>Prescurtare</th>
							<th>Nume</th>
						</tr>

						<!-- loop over and print our discipline -->
						<c:forEach var="tempDomeniu" items="${domenii}">

							<!-- construct an "update" link with disciplina id -->
							<c:url var="updateLink" value="/domspec/updateFormDomeniu">
								<c:param name="domeniuId" value="${tempDomeniu.idDomeniu}" />
							</c:url>

							<!-- construct an "delete" link with disciplina id -->
							<c:url var="deleteLink" value="/domspec/deleteDomeniu">
								<c:param name="domeniuId" value="${tempDomeniu.idDomeniu}" />
							</c:url>

							<tr>
								<td>${tempDomeniu.codDomeniu}</td>

								<td>${tempDomeniu.prescurtareD}</td>

								<td>${tempDomeniu.numeDomeniu}</td>

								<td>
									<!-- display the update link --> <a href="${updateLink}">Modifică</a>
									| <a href="${deleteLink}"
									onclick="if (!(confirm('Confirmați ștergerea?'))) return false">Șterge</a>
								</td>

							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>
	</div>
</div> <!-- end div domenii -->

<!-- TAB SPECIALIZARI -->
<div class="tab-pane fade" id="specializari" role="tabpanel" aria-labelledby="specializari-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />
			<input type="button" value="Adaugă Specializare"
				onclick="window.location.href='showForm'; return false;"
				class="btn btn-primary bg-admin" />
				<br/><br/>


			<div class="card card-info">
                <div class="card-header">
					<div class="card-title h2">Lista Specializărilor</div>
				</div>

				<div class="card-body">
                    <div class="container">
                        <!-- loop pentru domenii // constructie tab headers -->
                        <ul class="nav nav-tabs" id="TabHeaderDomenii" role="tablist">
					    	<c:forEach var="tempDomeniu" items="${domenii}">
					        <li class="nav-item">
					            <a class="nav-link active color-admin" id="${tempDomeniu.prescurtareD}" data-toggle="tab" href="#${tempDomeniu.prescurtareD}" role="tab" aria-controls="${tempDomeniu.prescurtareD}" aria-selected="false">${tempDomeniu.prescurtareD}</a>
					        </li>
						    </c:forEach>
					    </ul>
					 </div>


					<div class="tab-content" id="TabContentDomenii">

					<!-- outer loop pentru domenii // constructie tab content -->
					<c:forEach var="tempDomeniu" items="${domenii}">
    					<div class="tab-pane fade show active" id="${tempDomeniu.prescurtareD}" role="tabpanel" aria-labelledby="${tempDomeniu.prescurtareD}">
                            <div class="container">
                                <div class="col-md-offset-1 col-md-10">
                                    <hr />
                                    <table class="table table-striped table-bordered">
        								<tr>
        									<th>Cod Specializare</th>
        									<th>Prescurtare</th>
        									<th>Nume</th>
        								</tr>

        								<!-- inner loop pentru specializari -->
        								<c:forEach var="tempSpecializare" items="${tempDomeniu.specializariCollection}">

        									<!-- construct an "update" link with disciplina id -->
        									<c:url var="updateLink" value="/domspec/updateFormSpecializare">
        										<c:param name="SpecializareId" value="${tempSpecializare.idSpecializare}" />
        									</c:url>

        									<!-- construct an "delete" link with disciplina id -->
        									<c:url var="deleteLink" value="/domspec/deleteSpecializare">
        										<c:param name="SpecializareId" value="${tempSpecializare.idSpecializare}" />
        									</c:url>

        									<tr>
        										<td>${tempSpecializare.codSpecializare}</td>
        										<td>${tempSpecializare.prescurtareS}</td>
        										<td>${tempSpecializare.numeSpecializare}</td>
        										<td>
        											<!-- display the update link --><a href="${updateLink}">Modifică</a>
        											| <a href="${deleteLink}"
        											onclick="if (!(confirm('Confirmați ștergerea?'))) return false">Șterge</a>
        										</td>
        									</tr>

                                        </c:forEach> <!-- endloop dom.specializari -->
                                    </table>
                                </div>
                            </div>
                        </div>
					</c:forEach> <!-- endloop domenii -->
                    </div> <!-- End of Tab Content -->

                </div> <!-- end of card-body -->
            </div> <!-- end of card card-info -->
        </div> <!-- end of col whatever -->
    </div> <!-- end of container -->
</div> <!-- end of tab pane SPECIALIZARI -->

</div> <!-- end tab content Parent -->

</body>

</html>
