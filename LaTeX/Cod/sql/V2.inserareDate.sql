INSERT INTO `planuri_invatamant`.`facultati`
	(`COD_FAC`, `NUME_FAC`, `PRESCURTARE_FAC`) VALUES
		(4, "ELECTRONICA, TELECOMUNICATII SI TEHNOLOGIA INFORMATIEI", "ETTI");


INSERT INTO `planuri_invatamant`.`domenii`
	(`COD_DOMENIU`, `PRESCURTARE_D`, `NUME_DOMENIU`) VALUES
		("100", "IETTI",
			"Inginerie electronica, telecomunicatii si tehnologii informationale"),
		("010", "CTI",
			"Calculatoare si tehnologia informatiei");


INSERT INTO `planuri_invatamant`.`specializari`
	(`ID_DOMENIU`, `COD_SPECIALIZARE`, `NUME_SPECIALIZARE`, `PRESCURTARE_S`) VALUES
		(1, "010", "Electronica aplicata", "ELA"),
        (1, "020", "Tehnologii si sisteme de telecomunicatii", "TST"),
        (1, "030", "Retele si software de telecomunicatii", "RST"),
        (1, "040", "Microelectronica, optoelectronica si nanotehnologii", "MON"),
        (2, "040", "Ingineria informatiei", "INF");

INSERT INTO `planuri_invatamant`.`specializari`
	(`ID_DOMENIU`, `COD_SPECIALIZARE`, `NUME_SPECIALIZARE`, `PRESCURTARE_S`, `LB_PREDARE`) VALUES
		(1, "010", "Electronica aplicata", "ELAen", "EN"),
        (1, "020", "Tehnologii si sisteme de telecomunicatii", "TSTen", "EN");

INSERT INTO `planuri_invatamant`.`discipline`
	(`COD_1`, `COD_2`, `COD_3`, `COD_4`, `COD_5`,
     `NUME_DISCIPLINA`,
     `ID_DOMENIU`, `ID_SPECIALIZARE`,
	 `ORE_SAPT_C`, `ORE_SAPT_S`,  `ORE_SAPT_L`,  `ORE_SAPT_P`, `ORE_SAPT_PW`,
     `ECTS`, `SAPTAMANI`, `TIP_EXAMINARE`)

		VALUES

		("04", "D", "05", "O", "001",
        "Teoria transmisiunii informatiei",
         1, 2,
         3, 1, 1, 0, 0,
         5, 14, "E"),


         ("04", "D", "06", "O", "001",
        "Information Transmission Theory",
         1, 3,
         3, 1, 1, 0, 0,
         5, 14, "E"),


		("04", "F", "01", "O", "001", "Analiza matematica 1",
        1, 2,
        3, 1, 0, 0, 0,
        4, 14, "E");


-- credentiale
-- parola = dev, hashed cu bcrypt
INSERT INTO `planuri_invatamant`.`utilizatori` (TIP_UTILIZATOR, NUME, PAROLA, EMAIL, ENABLED) VALUES
  ('admin', 'admin', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'admin_email@etti.pub.ro', 1),
  ('profesor', 'prof1', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'prof1_email@etti.pub.ro', 1),
  ('profesor', 'prof2', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'prof2_email@etti.pub.ro', 1),
  ('sef-specializare', 'sefRST', '$2a$10$GHLN4P4F/gAImYA5dQQqzOfcL8v7YxcIsQscTXThalvHAp1.Voi6a', 'sef-specializare_email@etti.pub.ro', 1);

INSERT INTO `planuri_invatamant`.`authority` (AUTH_NAME) VALUES ('ADMIN');
INSERT INTO `planuri_invatamant`.`authority` (AUTH_NAME) VALUES ('USER');

INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (1, 1);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (1, 2);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (2, 1);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (3, 1);
INSERT INTO `planuri_invatamant`.`user2authority` (ID_UTILIZATOR, AUTH_ID) VALUES (4, 1);
