package com.iosif.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class EroriController implements ErrorController {


    private int getErrorCode (HttpServletRequest httpRequest){
        return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
    }

    @RequestMapping("/error")
    public ModelAndView generarePagina(HttpServletRequest httpRequest) {

        ModelAndView errorPage = new ModelAndView("eroare");
        String mesajEroare = "";
        int httpErrorCode = getErrorCode(httpRequest);

        switch (httpErrorCode) {
            case 400: {
                mesajEroare = "Http Error Code: 400. Bad Request";
                break;
            }
            case 401: {
                mesajEroare = "Http Error Code: 401. Unauthorized";
                break;
            }
            case 404: {
                mesajEroare = "Http Error Code: 404. Resource not found";
                break;
            }

            case 405: {
                mesajEroare = "Http Error Code: 405. Method Not Allowed. Posibil CSRF evitat.";
                break;
            }

            case 500: {
                mesajEroare = "Http Error Code: 500. Internal Server Error";
                break;
            }
        }

        errorPage.addObject("mesajEroare", mesajEroare);
        return errorPage;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

}
