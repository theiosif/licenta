package com.iosif.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iosif.dao.DomspecDAO;
import com.iosif.entity.Specializare;
import com.iosif.entity.Domeniu;

@Service
public class DomspecServiceImpl implements DomspecService {

	@Autowired
	private DomspecDAO domspecDAO;
	
	@Override
	@Transactional
	public List<Domeniu> getDomenii() {
		return domspecDAO.getDomenii();
	}

	@Override
	@Transactional
	public void saveDomeniu(Domeniu theDomeniu) {
		domspecDAO.saveDomeniu(theDomeniu);
	}

	@Override
	@Transactional
	public Domeniu getDomeniu(int theId) {
		return domspecDAO.getDomeniu(theId);
	}

	@Override
	@Transactional
	public void deleteDomeniu(int theId) {
		domspecDAO.deleteDomeniu(theId);
	}
        
        // ---> XXX <--- \
                
        @Override
	@Transactional
	public List<Specializare> getSpecializari() {
		return domspecDAO.getSpecializari();
	}

	@Override
	@Transactional
	public void saveSpecializare(Specializare theSpecializare) {
		domspecDAO.saveSpecializare(theSpecializare);
	}

	@Override
	@Transactional
	public Specializare getSpecializare(int theId) {
		return domspecDAO.getSpecializare(theId);
	}

	@Override
	@Transactional
	public void deleteSpecializare(int theId) {
		domspecDAO.deleteSpecializare(theId);
	}
}





