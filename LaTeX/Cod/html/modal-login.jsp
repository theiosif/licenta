<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Continut Modal -->
        <div class="modal-content">

            <div class="modal-header">
                <h4><span class="glyphicon glyphicon-lock"></span>Autentificare</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body" >
                <form role="form" action="auth/login" method="post">
                    <div class="form-group">
                        <label for="usrname"><span class="glyphicon glyphicon-user"></span>Utilizator</label>
                        <input type="text" class="form-control" name="usrname"  id="usrname" placeholder="Introduceti numele de utilizator">
                    </div>
                    <div class="form-group">
                        <label for="psw"><span class="glyphicon glyphicon-eye-open"></span>Parola</label>
                        <input type="text" class="form-control" name="psw" id="psw" placeholder="Introduceti parola">
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" value="" checked>Pastreaza-ma autentificat</label>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <button type="submit" class="btn bg-admin btn-lg btn btn-outline-light"><span class="glyphicon glyphicon-off"></span> Login</button>
                </form>
            </div>

            <div class="modal-footer">
                <p><a href="#">Parola uitata?</a></p>
            </div>
        </div>

    </div>
</div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/modal.js"></script>