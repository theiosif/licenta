package com.iosif.entity;

import com.iosif.entity.Specializare;
import com.iosif.entity.Utilizator;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(User2specializare.class)
public class User2specializare_ { 

    public static volatile SingularAttribute<User2specializare, Utilizator> idUtilizator;
    public static volatile SingularAttribute<User2specializare, Specializare> idSpecializare;
    public static volatile SingularAttribute<User2specializare, Long> idDrept;

}