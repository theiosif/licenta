/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wips;

import com.model.*;
import com.dao.DisciplinaDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author yoji
 * 
 * Throwaway servlet, 
 * used for code snippet testing only
 */
public class wip_servlet extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
        
        String user = req.getParameter("user");
        String parola = req.getParameter("parola");

        PrintWriter out = res.getWriter();
        out.println("Ceaules baftalo " + user + " cu parola: " + parola);
                        
    }
}
