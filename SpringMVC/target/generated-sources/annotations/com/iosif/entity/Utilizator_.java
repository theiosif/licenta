package com.iosif.entity;

import com.iosif.entity.Sesiune;
import com.iosif.entity.User2disciplina;
import com.iosif.entity.User2domeniu;
import com.iosif.entity.User2specializare;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(Utilizator.class)
public class Utilizator_ { 

    public static volatile SingularAttribute<Utilizator, Integer> idUtilizator;
    public static volatile CollectionAttribute<Utilizator, User2domeniu> user2domeniuCollection;
    public static volatile CollectionAttribute<Utilizator, User2specializare> user2specializareCollection;
    public static volatile SingularAttribute<Utilizator, String> parola;
    public static volatile SingularAttribute<Utilizator, String> tipUtilizator;
    public static volatile CollectionAttribute<Utilizator, Sesiune> sesiuniCollection;
    public static volatile SingularAttribute<Utilizator, String> nume;
    public static volatile CollectionAttribute<Utilizator, User2disciplina> user2disciplinaCollection;

}