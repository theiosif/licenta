package com.iosif.service;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UtilizatorDAO UtilizatorDAO;
    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {
        Utilizator utilizatorActiv = UtilizatorDAO.findByNume(userName);
        
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

        // work-around din cauza modificarilor
        // aparute in Spring Security ver.5
        Collection<Authority> authCollection= utilizatorActiv.getUserAuthority();
        String autorizari = authCollection.stream()
                .map(Authority::getAuthName)
                .collect(Collectors.joining(", "));

        String[] authStrings = autorizari.split(", ");
        for(String authString : authStrings) {
            authorities.add(new SimpleGrantedAuthority(authString));
        }

        UserDetails userDetails = (UserDetails)new User(utilizatorActiv.getNume(),
                utilizatorActiv.getParola(), authorities);

        return userDetails;
    }
}