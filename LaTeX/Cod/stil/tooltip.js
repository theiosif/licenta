$(document).ready(function () {
    $("a").tooltip({
        'selector': '',
        'placement': 'top',
        'container':'body'
    });
});

$('.auto-tooltip').tooltip();

$("ul.nav-tabs a").click(function (e) {
    e.preventDefault();
    $(this).tab('show');
});
