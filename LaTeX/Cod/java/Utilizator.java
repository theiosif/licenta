package com.iosif.entity;

@Entity
@Audited
@Table(name = "utilizatori")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Utilizator.findAll", query = "SELECT u FROM Utilizator u"),
    @NamedQuery(name = "Utilizator.findByIdUtilizator", query = "SELECT u FROM Utilizator u WHERE u.idUtilizator = :idUtilizator"),
    @NamedQuery(name = "Utilizator.findByTipUtilizator", query = "SELECT u FROM Utilizator u WHERE u.tipUtilizator = :tipUtilizator"),
    @NamedQuery(name = "Utilizator.findByNume", query = "SELECT u FROM Utilizator u WHERE u.nume = :nume"),
    @NamedQuery(name = "Utilizator.findByParola", query = "SELECT u FROM Utilizator u WHERE u.parola = :parola")})
public class Utilizator implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_UTILIZATOR")
    private Integer idUtilizator;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "TIP_UTILIZATOR")
    private String tipUtilizator;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NUME")
    private String nume;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "PAROLA")
    private String parola;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "ENABLED")
    private boolean enabled;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<User2domeniu> user2domeniuCollection;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<Sesiune> sesiuniCollection;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<User2specializare> user2specializareCollection;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<User2disciplina> user2disciplinaCollection;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user2authority", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Collection<Authority> Autoritati;

    public Utilizator() {
    }

    public Utilizator(Integer idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public Utilizator(Integer idUtilizator, String tipUtilizator, String nume, String parola) {
        this.idUtilizator = idUtilizator;
        this.tipUtilizator = tipUtilizator;
        this.nume = nume;
        this.parola = parola;
    }

    public Integer getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(Integer idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public String getTipUtilizator() {
        return tipUtilizator;
    }

    public void setTipUtilizator(String tipUtilizator) {
        this.tipUtilizator = tipUtilizator;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @XmlTransient
    public Collection<User2domeniu> getUser2domeniuCollection() {
        return user2domeniuCollection;
    }

    public void setUser2domeniuCollection(Collection<User2domeniu> user2domeniuCollection) {
        this.user2domeniuCollection = user2domeniuCollection;
    }

    @XmlTransient
    public Collection<Sesiune> getSesiuniCollection() {
        return sesiuniCollection;
    }

    public void setSesiuniCollection(Collection<Sesiune> sesiuniCollection) {
        this.sesiuniCollection = sesiuniCollection;
    }

    @XmlTransient
    public Collection<User2specializare> getUser2specializareCollection() {
        return user2specializareCollection;
    }

    public void setUser2specializareCollection(Collection<User2specializare> user2specializareCollection) {
        this.user2specializareCollection = user2specializareCollection;
    }

    @XmlTransient
    public Collection<User2disciplina> getUser2disciplinaCollection() {
        return user2disciplinaCollection;
    }

    public void setUser2disciplinaCollection(Collection<User2disciplina> user2disciplinaCollection) {
        this.user2disciplinaCollection = user2disciplinaCollection;
    }

    public Collection<Authority> getUserAuthority() {
        return Autoritati;
    }

    public void setUserAuthority(Collection<Authority> user2authorityCollection) {
        this.Autoritati = user2authorityCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUtilizator != null ? idUtilizator.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilizator)) {
            return false;
        }
        Utilizator other = (Utilizator) object;
        if ((this.idUtilizator == null && other.idUtilizator != null) || (this.idUtilizator != null && !this.idUtilizator.equals(other.idUtilizator))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.Utilizatori[ idUtilizator=" + idUtilizator + " ]";
    }
    
}
