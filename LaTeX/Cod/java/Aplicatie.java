package com.iosif.aplicatie;

@EnableAutoConfiguration//(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@ComponentScan("com.iosif")
@EntityScan("com.iosif.entity")
@EnableJpaRepositories("com.iosif.dao")
@EnableJpaAuditing
@EnableTransactionManagement
public class Aplicatie {
   public static void main(String[] args) {
      SpringApplication.run(Aplicatie.class, args);
   }
}