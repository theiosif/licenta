/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author yoji
 */
public class DBConnection {
    
    public static Connection getConnection(){
            Connection con=null;
            try{
                    Class.forName("com.mysql.jdbc.Driver");
                    con=DriverManager.getConnection("jdbc:mysql://localhost:3306/planuri_invatamant",
                            "yoji",
                            "mysqlpass");
            }catch(Exception e){System.out.println(e);}
            return con;
    }

    public static void disconnect(Connection con) throws SQLException {
        if (con != null && !con.isClosed()) {
            con.close();
        }
    }
    
}
