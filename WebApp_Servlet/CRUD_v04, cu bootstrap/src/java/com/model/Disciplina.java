/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yoji
 */
@Entity
@Table(name = "discipline")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Discipline.findAll", query = "SELECT d FROM Discipline d")
    , @NamedQuery(name = "Discipline.findByIdDisciplina", query = "SELECT d FROM Discipline d WHERE d.idDisciplina = :idDisciplina")
    , @NamedQuery(name = "Discipline.findByCod1", query = "SELECT d FROM Discipline d WHERE d.cod1 = :cod1")
    , @NamedQuery(name = "Discipline.findByCod2", query = "SELECT d FROM Discipline d WHERE d.cod2 = :cod2")
    , @NamedQuery(name = "Discipline.findByCod3", query = "SELECT d FROM Discipline d WHERE d.cod3 = :cod3")
    , @NamedQuery(name = "Discipline.findByCod4", query = "SELECT d FROM Discipline d WHERE d.cod4 = :cod4")
    , @NamedQuery(name = "Discipline.findByCod5", query = "SELECT d FROM Discipline d WHERE d.cod5 = :cod5")
    , @NamedQuery(name = "Discipline.findByNumeDisciplina", query = "SELECT d FROM Discipline d WHERE d.numeDisciplina = :numeDisciplina")
    , @NamedQuery(name = "Discipline.findByLbPredare", query = "SELECT d FROM Discipline d WHERE d.lbPredare = :lbPredare")
    , @NamedQuery(name = "Discipline.findByEcts", query = "SELECT d FROM Discipline d WHERE d.ects = :ects")
    , @NamedQuery(name = "Discipline.findByOreSaptC", query = "SELECT d FROM Discipline d WHERE d.oreSaptC = :oreSaptC")
    , @NamedQuery(name = "Discipline.findByOreSaptS", query = "SELECT d FROM Discipline d WHERE d.oreSaptS = :oreSaptS")
    , @NamedQuery(name = "Discipline.findByOreSaptL", query = "SELECT d FROM Discipline d WHERE d.oreSaptL = :oreSaptL")
    , @NamedQuery(name = "Discipline.findByOreSaptP", query = "SELECT d FROM Discipline d WHERE d.oreSaptP = :oreSaptP")
    , @NamedQuery(name = "Discipline.findByOreSaptPw", query = "SELECT d FROM Discipline d WHERE d.oreSaptPw = :oreSaptPw")
    , @NamedQuery(name = "Discipline.findBySaptamani", query = "SELECT d FROM Discipline d WHERE d.saptamani = :saptamani")
    , @NamedQuery(name = "Discipline.findByTipExaminare", query = "SELECT d FROM Discipline d WHERE d.tipExaminare = :tipExaminare")})
public class Disciplina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DISCIPLINA")
    private Integer idDisciplina;
    @Basic(optional = false)
    @Column(name = "COD_1")
    private String cod1;
    @Basic(optional = false)
    @Column(name = "COD_2")
    private Character cod2;
    @Basic(optional = false)
    @Column(name = "COD_3")
    private String cod3;
    @Basic(optional = false)
    @Column(name = "COD_4")
    private Character cod4;
    @Basic(optional = false)
    @Column(name = "COD_5")
    private String cod5;
    @Basic(optional = false)
    @Column(name = "NUME_DISCIPLINA")
    private String numeDisciplina;
    @Basic(optional = false)
    @Column(name = "LB_PREDARE")
    private String lbPredare;
    @Basic(optional = false)
    @Column(name = "ECTS")
    private short ects;
    @Basic(optional = false)
    @Column(name = "ORE_SAPT_C")
    private short oreSaptC;
    @Basic(optional = false)
    @Column(name = "ORE_SAPT_S")
    private short oreSaptS;
    @Basic(optional = false)
    @Column(name = "ORE_SAPT_L")
    private short oreSaptL;
    @Basic(optional = false)
    @Column(name = "ORE_SAPT_P")
    private short oreSaptP;
    @Basic(optional = false)
    @Column(name = "ORE_SAPT_PW")
    private short oreSaptPw;
    @Column(name = "SAPTAMANI")
    private Integer saptamani;
    @Column(name = "TIP_EXAMINARE")
    private String tipExaminare;
    @JoinColumn(name = "ID_DOMENIU", referencedColumnName = "ID_DOMENIU")
    @ManyToOne(optional = false)
    private Short idDomeniu;
    @JoinColumn(name = "ID_SPECIALIZARE", referencedColumnName = "ID_SPECIALIZARE")
    @ManyToOne(optional = false)
    private Short idSpecializare;

    public Disciplina() {
    }

    public Disciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public Disciplina(Integer idDisciplina, String cod1, Character cod2, String cod3, Character cod4, String cod5, String numeDisciplina, String lbPredare, short ects, short oreSaptC, short oreSaptS, short oreSaptL, short oreSaptP, short oreSaptPw) {
        this.idDisciplina = idDisciplina;
        this.cod1 = cod1;
        this.cod2 = cod2;
        this.cod3 = cod3;
        this.cod4 = cod4;
        this.cod5 = cod5;
        this.numeDisciplina = numeDisciplina;
        this.lbPredare = lbPredare;
        this.ects = ects;
        this.oreSaptC = oreSaptC;
        this.oreSaptS = oreSaptS;
        this.oreSaptL = oreSaptL;
        this.oreSaptP = oreSaptP;
        this.oreSaptPw = oreSaptPw;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public String getCod1() {
        return cod1;
    }

    public void setCod1(String cod1) {
        this.cod1 = cod1;
    }

    public Character getCod2() {
        return cod2;
    }

    public void setCod2(Character cod2) {
        this.cod2 = cod2;
    }

    public String getCod3() {
        return cod3;
    }

    public void setCod3(String cod3) {
        this.cod3 = cod3;
    }

    public Character getCod4() {
        return cod4;
    }

    public void setCod4(Character cod4) {
        this.cod4 = cod4;
    }

    public String getCod5() {
        return cod5;
    }

    public void setCod5(String cod5) {
        this.cod5 = cod5;
    }

    public String getNumeDisciplina() {
        return numeDisciplina;
    }

    public void setNumeDisciplina(String numeDisciplina) {
        this.numeDisciplina = numeDisciplina;
    }

    public String getLbPredare() {
        return lbPredare;
    }

    public void setLbPredare(String lbPredare) {
        this.lbPredare = lbPredare;
    }

    public short getEcts() {
        return ects;
    }

    public void setEcts(short ects) {
        this.ects = ects;
    }

    public short getOreSaptC() {
        return oreSaptC;
    }

    public void setOreSaptC(short oreSaptC) {
        this.oreSaptC = oreSaptC;
    }

    public short getOreSaptS() {
        return oreSaptS;
    }

    public void setOreSaptS(short oreSaptS) {
        this.oreSaptS = oreSaptS;
    }

    public short getOreSaptL() {
        return oreSaptL;
    }

    public void setOreSaptL(short oreSaptL) {
        this.oreSaptL = oreSaptL;
    }

    public short getOreSaptP() {
        return oreSaptP;
    }

    public void setOreSaptP(short oreSaptP) {
        this.oreSaptP = oreSaptP;
    }

    public short getOreSaptPw() {
        return oreSaptPw;
    }

    public void setOreSaptPw(short oreSaptPw) {
        this.oreSaptPw = oreSaptPw;
    }

    public Integer getSaptamani() {
        return saptamani;
    }

    public void setSaptamani(Integer saptamani) {
        this.saptamani = saptamani;
    }

    public String getTipExaminare() {
        return tipExaminare;
    }

    public void setTipExaminare(String tipExaminare) {
        this.tipExaminare = tipExaminare;
    }

    public Short getIdDomeniu() {
        return idDomeniu;
    }

    public void setIdDomeniu(Short idDomeniu) {
        this.idDomeniu = idDomeniu;
    }

    public Short getIdSpecializare() {
        return idSpecializare;
    }

    public void setIdSpecializare(Short idSpecializare) {
        this.idSpecializare = idSpecializare;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDisciplina != null ? idDisciplina.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Disciplina)) {
            return false;
        }
        Disciplina other = (Disciplina) object;
        if ((this.idDisciplina == null && other.idDisciplina != null) || (this.idDisciplina != null && !this.idDisciplina.equals(other.idDisciplina))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.controller.Discipline[ idDisciplina=" + idDisciplina + " ]";
    }
    
}
