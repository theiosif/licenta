package com.iosif.service;

import java.util.List;

import com.iosif.entity.Disciplina;

public interface DisciplinaService {

	public List<Disciplina> findAllDiscipline();
        
	public Disciplina updateDisciplina(Disciplina appointment);
        
	public Disciplina saveDisciplina(Disciplina appointment);
        
	public void deleteDisciplinaById(Integer id);

	public Disciplina getDisciplina(Integer id);
}