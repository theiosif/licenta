package com.iosif.service;

import java.util.List;

import com.iosif.entity.Specializare;

public interface SpecializareService {

	public List<Specializare> findAllSpecializari();
        
	public Specializare updateSpecializare(Specializare appointment);
        
	public Specializare saveSpecializare(Specializare appointment);
        
	public void deleteSpecializareById(Short id);
        
        public Specializare getSpecializare(Short Id);
}