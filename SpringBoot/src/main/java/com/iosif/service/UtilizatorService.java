package com.iosif.service;

import com.iosif.entity.Authority;
import com.iosif.entity.Utilizator;
import com.iosif.dao.AuthorityDAO;
import com.iosif.dao.UtilizatorDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

@Service("UtilizatorService")
public class UtilizatorService {

    private UtilizatorDAO UtilizatorDAO;
    private AuthorityDAO AuthorityDAO;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UtilizatorService(UtilizatorDAO UtilizatorDAO,
                       AuthorityDAO AuthorityDAO,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.UtilizatorDAO = UtilizatorDAO;
        this.AuthorityDAO = AuthorityDAO;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public Utilizator findUtilizatorByNume(String nume) {
        return UtilizatorDAO.findByNume(nume);
    }

    public void saveUtilizator(Utilizator Utilizator) {
        Utilizator.setParola(bCryptPasswordEncoder.encode(Utilizator.getParola()));
        Utilizator.setEnabled(true);
        Authority UtilizatorAuthority = AuthorityDAO.findByAuthName("ADMIN");
        Utilizator.setUserAuthority(new HashSet<Authority>(Arrays.asList(UtilizatorAuthority)));
        UtilizatorDAO.save(Utilizator);
    }

}