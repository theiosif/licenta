#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 04:13:49 2018

@author: yoji
"""

import pandas as pd
import numpy as np

FILENAME = 'raw_with_deletions.csv'

raw = pd.read_csv(FILENAME, header=None)

# drop "header"
data = pd.DataFrame(raw.iloc[3:].reset_index(drop=True))

# LEGENDA COLOANE
# 0 nume
# 1-5 COD1..5
# 6,7 ECTS
# 24, 25 - Tip Examinare
#
#   C  S   L    P    PW
#   8, 9 , 10 , 11 , 12  <- sem1
#  13, 14, 15 , 16, 17   <- sem2

# In[delete non-materii]:

data = data.dropna(subset=[1, 2, 3, 4, 5])
data = data.fillna(0)

# In[fix ects]:

for ects in range(6, 8):
    data[ects] = data[ects].replace(" ", 0)

data[6] = data[6].astype(float) + data[7].astype(float)
data = data.drop(columns=7)


# In[fix ore sapt]:
for nr_ore in range(8, 13):
    data[nr_ore] = data[nr_ore].astype(float)+data[nr_ore+5].astype(float)


# In[fix tip examinare]
# Rezolvam stringurile
for belea in [24, 25]:
    data[belea] = data[belea].replace("-", "")
    data[belea] = data[belea].replace("0", "")
    data[belea] = data[belea].replace(0, "")
# In[]:
data[24] = str(data[24]) + str(data[25])


# In[Finishing touches]:
dict_rename = {0: "Nume",
               1: "COD_1",
               2: "COD_2",
               3: "COD_3",
               4: "COD_4",
               5: "COD_5",
               6: "ECTS",
               8: "ORE_C",
               9: "ORE_S",
               10: "ORE_L",
               11: "ORE_P",
               12: "ORE_PW",
               18: "NR_SAPT",
               24: "METODA_EXAM"}

coloane = dict_rename.keys()
col_noi = dict_rename.values()

data = data.rename(columns=dict_rename)
#data = pd.DataFrame(data, columns=col_noi2)

#data = pd.DataFrame(data, columns=coloane)
