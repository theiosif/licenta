<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Adauga/Modifica Domeniu</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

</head>
<body>
	<div class="container">
		<div class="col-md-offset-2 col-md-7">
			<h2 class="text-center">Adauga/Modifica Domeniu</h2>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Formular Domeniu</div>
				</div>
				<div class="panel-body">
					<form:form action="saveDomeniu" cssClass="form-horizontal"
						method="post" modelAttribute="domeniu">

						<form:hidden path="idDomeniu" />

						<div class="form-group">
							<label for="idFac" class="col-md-3 control-label">ID Facultate</label>
							<div class="col-md-9">
								<form:input path="idFac.idFac" cssClass="form-control"  value="1"/>
							</div>
						</div>

						<div class="form-group">
							<label for="cod" class="col-md-3 control-label">Cod</label>
							<div class="col-md-9">
								<form:input path="codDomeniu" cssClass="form-control"  value="001"/>
							</div>
						</div>

						<div class="form-group">
							<label for="numeDomeniu" class="col-md-3 control-label">Nume</label>
							<div class="col-md-9">
								<form:input path="numeDomeniu" cssClass="form-control"  value="Securitate Digitala"/>
							</div>
						</div>

						<div class="form-group">
							<label for="prescurtareD" class="col-md-3 control-label">Prescurtare</label>
							<div class="col-md-9">
								<form:input path="PrescurtareD" cssClass="form-control"  value="SEC"/>
							</div>
						</div>

						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<form:button cssClass="btn btn-primary">Confirma</form:button>
							</div>
						</div>

					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>