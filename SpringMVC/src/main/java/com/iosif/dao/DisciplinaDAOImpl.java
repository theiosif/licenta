package com.iosif.dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iosif.entity.Disciplina;

@Repository
public class DisciplinaDAOImpl implements DisciplinaDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Disciplina> getDiscipline() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Disciplina> cq = cb.createQuery(Disciplina.class);
		Root<Disciplina> root = cq.from(Disciplina.class);
		cq.select(root);
		Query query = session.createQuery(cq);
		return query.getResultList();
	}

	@Override
	public void deleteDisciplina(int id) {
		Session session = sessionFactory.getCurrentSession();
		Disciplina book = session.byId(Disciplina.class).load(id);
		session.delete(book);
	}

	@Override
	public void saveDisciplina(Disciplina theDisciplina) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(theDisciplina);
	}

	@Override
	public Disciplina getDisciplina(int theId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Disciplina theDisciplina = currentSession.get(Disciplina.class, theId);
		return theDisciplina;
	}
}

