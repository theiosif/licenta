package com.iosif.config;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
// HTTPS-only
    @Value("${server.port.http}") //Din application.properties
        int httpPort;
    @Value("${server.port}") //Din application.properties
            int httpsPort;
// Redirectionare la HTTPS
    @Bean
    public WebFilter httpsRedirectFilter() {
        return new WebFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
                URI originalUri = exchange.getRequest().getURI();

                //here set your condition to http->https redirect
                List<String> forwardedValues = exchange.getRequest().getHeaders().get("x-forwarded-proto");
                if (forwardedValues != null && forwardedValues.contains("http")) {
                    try {
                        URI mutatedUri = new URI("https",
                                originalUri.getUserInfo(),
                                originalUri.getHost(),
                                originalUri.getPort(),
                                originalUri.getPath(),
                                originalUri.getQuery(),
                                originalUri.getFragment());
                        ServerHttpResponse response = exchange.getResponse();
                        response.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                        response.getHeaders().setLocation(mutatedUri);
                        return Mono.empty();
                    } catch (URISyntaxException e) {
                        throw new IllegalStateException(e.getMessage(), e);
                    }
                }
                return chain.filter(exchange);
            }
        };
    }

//Specificare SaltedHash pentru stocarea parolelor
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") //pentru IntelliJ
    @Autowired
    private DataSource dataSource;


    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("{spring.queries.roles-query}")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.
                jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                //.userDetailsService(CustomUserDetailsService())
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //redirect to HTTPS
        http.portMapper()
                .http(httpPort) // http port defined in yml config file
                .mapsTo(httpsPort); // https port defined in yml config file

        // antete XSS
        http.headers()
                .frameOptions().sameOrigin().xssProtection().block(false).xssProtectionEnabled(true);

        // CSP
        http.headers().contentSecurityPolicy(
  "script-src 'self' https://maxcdn.bootstrapcdn.com https://ajax.googleapis.com https://cdnjs.cloudflare.com;" +
                "object-src 'self' https://fonts.googleapis.com https://maxcdn.bootstrapcdn.com;" +
                "style-src 'self' https://fonts.googleapis.com https://maxcdn.bootstrapcdn.com;" +
                "report-uri /TODO/endpoint-raportare-incident");

        //Session Hijack cu Istoric
        http.headers().cacheControl();

        //Politica origine
        http.headers().referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.SAME_ORIGIN);


        http.authorizeRequests()
            .antMatchers("/", "/disciplina/*", "/auth/login").permitAll()
        .and()

        .formLogin()
            .loginPage("/")
            .usernameParameter("usrname")
            .passwordParameter("psw")
            .loginProcessingUrl("/auth/login")
            .defaultSuccessUrl("/domspec/lista")
            .failureUrl("/login?error=true")
            .permitAll()
        .and()

            .logout()
            /* .addLogoutHandler(new HeaderWriterLogoutHandler(
                    new ClearSiteDataHeaderWriter("*")));*/
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            //.logoutUrl("/logout")
            .logoutSuccessUrl("/")
            .clearAuthentication(true)

            .deleteCookies("JSESSIONID")
            .invalidateHttpSession(true)
            .permitAll()

        .and()
            .exceptionHandling() //exception handling configuration
            .accessDeniedPage("/app/error")

        .and()
        .rememberMe().rememberMeParameter("remember-me-new").key("uniqueAndSecret").tokenValiditySeconds(86400);

        http.authorizeRequests().antMatchers("/domspec/*").hasAnyAuthority("admin", "user").anyRequest().authenticated();
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
            .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "*.css", "*.js");
    }

}
