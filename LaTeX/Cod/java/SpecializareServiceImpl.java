package com.iosif.service;

@Service
public class SpecializareServiceImpl implements SpecializareService {


	@Autowired
	private SpecializareDAO specializareDAO;

	public List<Specializare> findAllSpecializari() {
		return specializareDAO.findAll();
	}

	public Specializare updateSpecializare(Specializare appointment) {
		return specializareDAO.saveAndFlush(appointment);
	}

	public Specializare saveSpecializare(Specializare appointment) {
		return specializareDAO.save(appointment);
	}

	public void deleteSpecializareById(Short id) {
		specializareDAO.deleteById(id);
	}
        
        public Specializare getSpecializare(Short Id){
                return specializareDAO.getOne(Id);
        }

}





