<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modifică Disciplină</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

</head>
<body>
	<div class="container">
		<div class="col-md-offset-2 col-md-7">
			<h2 class="text-center">Modifică Disciplină</h2>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Formular Disciplină</div>
				</div>
				<div class="panel-body">
					<form:form action="saveDisciplina" class="form-horizontal"
						method="post" modelAttribute="disciplina">

						<!-- need to associate this data with customer id -->
						<form:hidden path="idDisciplina" />

						<div class="form-group">
							<label for="idFac" class="col-md-3 control-label">Facultate</label>
							<div class="col-md-9">
								<form:input path="idFac.idFac" class="form-control" value="1"/>
							</div>
						</div>

						<div class="form-group">
							<label for="idDomeniu" class="col-md-3 control-label">Domeniu</label>
							<div class="col-md-9">
								<form:input path="idDomeniu.idDomeniu" class="form-control" value="3"/>
							</div>
						</div>
						<div class="form-group">
							<label for="idSpecializare" class="col-md-3 control-label">Specializare</label>
							<div class="col-md-9">
								<form:input path="idSpecializare.idSpecializare" class="form-control" value="8"/>
							</div>
						</div>
						<div class="form-group">
							<label for="cod1" class="col-md-3 control-label">Cod 1 (2)</label>
							<div class="col-md-9">
								<form:input path="cod1" class="form-control"  value="01"/>
							</div>
						</div>
                                                <div class="form-group">
							<label for="cod2" class="col-md-3 control-label">Cod 2 (1)</label>
							<div class="col-md-9">
								<form:input path="cod2" class="form-control" value="1"/>
							</div>
						</div>
						<div class="form-group">
							<label for="cod3" class="col-md-3 control-label">Cod 3 (2)</label>
							<div class="col-md-9">
								<form:input path="cod3" class="form-control" value="1"/>
							</div>
						</div>
						<div class="form-group">
							<label for="cod4" class="col-md-3 control-label">Cod 4 (1)</label>
							<div class="col-md-9">
								<form:input path="cod4" class="form-control" value="1"/>
							</div>
						</div>
                                                <div class="form-group">
							<label for="cod5" class="col-md-3 control-label">Cod 5 (3)</label>
							<div class="col-md-9">
								<form:input path="cod5" class="form-control" value="010"/>
							</div>
						</div>
							<div class="form-group">
							<label for="lbPredare" class="col-md-3 control-label">Limba Predării</label>
							<div class="col-md-9">
								<form:input path="lbPredare" class="form-control" value="RO"/>
							</div>
						</div>
                                                

						<div class="form-group">
							<label for="numeDisciplina" class="col-md-3 control-label">Nume</label>
							<div class="col-md-9">
								<form:input path="numeDisciplina" class="form-control" value="Criptografie Post-Quantum"/>
							</div>
						</div>

						<div class="form-group">
							<label for="numeDisciplina" class="col-md-3 control-label">Puncte de Credit</label>
							<div class="col-md-9">
								<form:input path="ects" class="form-control"  value="99"/>
							</div>
						</div>

						<div class="form-group">
							<label for="numeDisciplina" class="col-md-3 control-label">Examinare</label>
							<div class="col-md-9">
								<form:input path="tipExaminare" class="form-control" value="E"/>
							</div>
						</div>


						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<form:button class="btn btn-lg btn-primary">Confirmă</form:button>
							</div>
						</div>

					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>