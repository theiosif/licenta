/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iosif.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author yoji
 */
@Entity
@Table(name = "domenii")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Domeniu.findAll", query = "SELECT d FROM Domeniu d"),
    @NamedQuery(name = "Domeniu.findByIdDomeniu", query = "SELECT d FROM Domeniu d WHERE d.idDomeniu = :idDomeniu"),
    @NamedQuery(name = "Domeniu.findByCodDomeniu", query = "SELECT d FROM Domeniu d WHERE d.codDomeniu = :codDomeniu"),
    @NamedQuery(name = "Domeniu.findByNumeDomeniu", query = "SELECT d FROM Domeniu d WHERE d.numeDomeniu = :numeDomeniu"),
    @NamedQuery(name = "Domeniu.findByPrescurtareD", query = "SELECT d FROM Domeniu d WHERE d.prescurtareD = :prescurtareD")})
public class Domeniu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DOMENIU")
    private Short idDomeniu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "COD_DOMENIU")
    private String codDomeniu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "NUME_DOMENIU")
    private String numeDomeniu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "PRESCURTARE_D")
    private String prescurtareD;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDomeniu", fetch=FetchType.EAGER)
    private Collection<Specializare> specializariCollection;
    @OneToMany(mappedBy = "idDomeniu")
    private Collection<User2domeniu> user2domeniuCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDomeniu")
    private Collection<Disciplina> disciplineCollection;

    public Domeniu() {
    }

    public Domeniu(Short idDomeniu) {
        this.idDomeniu = idDomeniu;
    }

    public Domeniu(Short idDomeniu, String codDomeniu, String numeDomeniu, String prescurtareD) {
        this.idDomeniu = idDomeniu;
        this.codDomeniu = codDomeniu;
        this.numeDomeniu = numeDomeniu;
        this.prescurtareD = prescurtareD;
    }

    public Short getIdDomeniu() {
        return idDomeniu;
    }

    public void setIdDomeniu(Short idDomeniu) {
        this.idDomeniu = idDomeniu;
    }

    public String getCodDomeniu() {
        return codDomeniu;
    }

    public void setCodDomeniu(String codDomeniu) {
        this.codDomeniu = codDomeniu;
    }

    public String getNumeDomeniu() {
        return numeDomeniu;
    }

    public void setNumeDomeniu(String numeDomeniu) {
        this.numeDomeniu = numeDomeniu;
    }

    public String getPrescurtareD() {
        return prescurtareD;
    }

    public void setPrescurtareD(String prescurtareD) {
        this.prescurtareD = prescurtareD;
    }

    @XmlTransient
    public Collection<Specializare> getSpecializariCollection() {
        return specializariCollection;
    }

    public void setSpecializariCollection(Collection<Specializare> specializariCollection) {
        this.specializariCollection = specializariCollection;
    }

    @XmlTransient
    public Collection<User2domeniu> getUser2domeniuCollection() {
        return user2domeniuCollection;
    }

    public void setUser2domeniuCollection(Collection<User2domeniu> user2domeniuCollection) {
        this.user2domeniuCollection = user2domeniuCollection;
    }

    @XmlTransient
    public Collection<Disciplina> getDisciplineCollection() {
        return disciplineCollection;
    }

    public void setDisciplineCollection(Collection<Disciplina> disciplineCollection) {
        this.disciplineCollection = disciplineCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDomeniu != null ? idDomeniu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Domeniu)) {
            return false;
        }
        Domeniu other = (Domeniu) object;
        if ((this.idDomeniu == null && other.idDomeniu != null) || (this.idDomeniu != null && !this.idDomeniu.equals(other.idDomeniu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.Domeniu[ idDomeniu=" + idDomeniu + " ]";
    }
    
}
