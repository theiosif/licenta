/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iosif.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author yoji
 */
@Entity
@Table(name = "utilizatori")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Utilizator.findAll", query = "SELECT u FROM Utilizator u"),
    @NamedQuery(name = "Utilizator.findByIdUtilizator", query = "SELECT u FROM Utilizator u WHERE u.idUtilizator = :idUtilizator"),
    @NamedQuery(name = "Utilizator.findByTipUtilizator", query = "SELECT u FROM Utilizator u WHERE u.tipUtilizator = :tipUtilizator"),
    @NamedQuery(name = "Utilizator.findByNume", query = "SELECT u FROM Utilizator u WHERE u.nume = :nume"),
    @NamedQuery(name = "Utilizator.findByParola", query = "SELECT u FROM Utilizator u WHERE u.parola = :parola")})
public class Utilizator implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_UTILIZATOR")
    private Integer idUtilizator;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "TIP_UTILIZATOR")
    private String tipUtilizator;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NUME")
    private String nume;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "PAROLA")
    private String parola;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<User2domeniu> user2domeniuCollection;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<Sesiune> sesiuniCollection;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<User2specializare> user2specializareCollection;
    @OneToMany(mappedBy = "idUtilizator")
    private Collection<User2disciplina> user2disciplinaCollection;

    public Utilizator() {
    }

    public Utilizator(Integer idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public Utilizator(Integer idUtilizator, String tipUtilizator, String nume, String parola) {
        this.idUtilizator = idUtilizator;
        this.tipUtilizator = tipUtilizator;
        this.nume = nume;
        this.parola = parola;
    }

    public Integer getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(Integer idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public String getTipUtilizator() {
        return tipUtilizator;
    }

    public void setTipUtilizator(String tipUtilizator) {
        this.tipUtilizator = tipUtilizator;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    @XmlTransient
    public Collection<User2domeniu> getUser2domeniuCollection() {
        return user2domeniuCollection;
    }

    public void setUser2domeniuCollection(Collection<User2domeniu> user2domeniuCollection) {
        this.user2domeniuCollection = user2domeniuCollection;
    }

    @XmlTransient
    public Collection<Sesiune> getSesiuniCollection() {
        return sesiuniCollection;
    }

    public void setSesiuniCollection(Collection<Sesiune> sesiuniCollection) {
        this.sesiuniCollection = sesiuniCollection;
    }

    @XmlTransient
    public Collection<User2specializare> getUser2specializareCollection() {
        return user2specializareCollection;
    }

    public void setUser2specializareCollection(Collection<User2specializare> user2specializareCollection) {
        this.user2specializareCollection = user2specializareCollection;
    }

    @XmlTransient
    public Collection<User2disciplina> getUser2disciplinaCollection() {
        return user2disciplinaCollection;
    }

    public void setUser2disciplinaCollection(Collection<User2disciplina> user2disciplinaCollection) {
        this.user2disciplinaCollection = user2disciplinaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUtilizator != null ? idUtilizator.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilizator)) {
            return false;
        }
        Utilizator other = (Utilizator) object;
        if ((this.idUtilizator == null && other.idUtilizator != null) || (this.idUtilizator != null && !this.idUtilizator.equals(other.idUtilizator))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.Utilizatori[ idUtilizator=" + idUtilizator + " ]";
    }
    
}
