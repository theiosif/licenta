GRANT ALL PRIVILEGES ON *.* TO 'ducky'@'localhost' IDENTIFIED BY 'licenta_sql';
tomcat: ducky / licenta


To read:
reflections
serializable
interfaces [X]
marker interface


@TODO:
  add @Documented la tot, pentru javadocs


---> cacti
---> viteza Spring/Hibernate vs Java <--> performance
---> pw for A110: zywifi20153
---> ID_FACULTATI: primu grup din cod



* Persistency options:
  1. Hibernate
  2. mysqldump -p -u $USER planuri_invatamant > dump_$DATE.sql
  3. snapshots
  4. http://www.liquibase.org/quickstart <-- EZ PZ LEMONSQUEEZY


## __-- CUPRINS --__
#### 1. Introducere
* Motivatie
    * Personala
        * Learn fukin webdev
        * Learn Java
        * Implement security
    * Universala
        * Bagpl in birocratie

#### 2. Context
* State of the Art // cautat platforme de campus FOSS
* __diagrama de Context__
![1](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/NDE_Context_Diagram_%28vector%29.svg/1200px-NDE_Context_Diagram_%28vector%29.svg.png)
* interfete tipice


#### 3. Cerinte si Analiza a Sistemului
* 3.1 Specificare functionala
    * usecases, descriere functionalitati
* 3.2 Analiza de domeniu/context
    * Diagrama de clase
    * structura DB
* 3.3 Comportament extern
    * flux activitati Sistem (organigrama)


#### 4. Proiectare
* 4.1 Proiectare de nivel inalt
    * Modelul MVC
        * Ce e
        * De ce il vrem
        * Cine e M/V/C la noi
    * Frameworkuri
        * __Start off with WHY__
        * Maven
        * Spring Boot
        * Spring MVC
        * Hibernate
* [?] 4.2 Programare interfata
    * RESTful web services
    * API decisions
* [?] 4.3 Pattern-uri de proiectare prezente
    * MVC
    * Factory
    * naiba stie, vedem cand se scrie codu


* 5. Implementare
    * Organizarea codului
    * __Desciere si exemple coduri semnificative__
    * Securizarea aplicatiei
        * OWASP TOP10
            * Salted hash
            * CSRF
            * XSS
            * SQLI


SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='numetabel';
https://www.dineshonjava.com/spring-crud-example-using-many-to-one/

#### Crearea unui utilizator pentru sistemul de gestiune a bazei de date
Autentificare in SGBD cu contul de _root_ cu comanda `mysql -u  root -p` (parola implicita este __root__), urmat de comenzile:

    CREATE USER 'dev'@'localhost' IDENTIFIED BY 'dev';
    GRANT ALL PRIVILEGES ON * . * TO 'dev'@'localhost';
    FLUSH PRIVILEGES;


#### Smecherii legate de LaTeX:
* https://www.doi2bib.org/
    * paste the DOI, get the ref
* sudo apt-get install texlive-bibtex-extra biber
*


### Migrare SpringMVC --> Spring Boot:
sters 2 clase din com.iosif.config
a disparut pachetul de `service`
tot ce e DAO e autoconfigurat prin mosteniri
database.properties --> application.properties (plus modificari in el)
`"spring.jpa.properties.` inainte de orice era legat de `Hibernate`
---
There is often quite a bit of confusion about the differences between JavaBeans and POJOs. The terms tend to be used interchangeably, but that’s not always accurate. JavaBeans are best characterized as a special kind of POJO. Put simply, a JavaBean is a POJO that follows three simple conventions:
• It is serializable.
• It has a public, a default, and a no argument constructor.
• It contains public getters and setters for each property that is to be read or written, respectively (write permissions can be obscured simply by defining a getter, without defining a setter).
---
Dependency injection (DI): Classes that employ dependency injection specify the objects that they interact with through constructor arguments, factory method parameters, or public mutators (aka setters). With a dependency-injection container or framework like Spring, the ability to externalize simple class properties is just the beginning. Developers can create a complex tree of dependencies, leaving the work of figuring out how each dependency is created and set (also called injected or wired) to the Spring lightweight container. Inversion of Control (IoC): When object location or instantiation is removed as a responsibility for a given bean and instead left to the framework, control has been inverted. This inversion of control is a very powerful concept and represents the foundation on which the Spring Framework is based.


NAMED PARAMETERS IN JPQL: o sa se astepte la numele dat de parametrii din functie! Attencion!

XSS:
// https://docs.spring.io/spring-security/site/docs/current/reference/html/default-security-headers-2.html#webflux-headers-xss-protection
.contentSecurityPolicy("script-src 'self' https://trustedscripts.example.com; object-src https://trustedplugins.example.com; report-uri /csp-report-endpoint/");
