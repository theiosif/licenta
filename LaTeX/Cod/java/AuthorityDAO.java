package com.iosif.dao;

@Repository
public interface AuthorityDAO extends JpaRepository<Authority, Integer> {
    Authority findByAuthName(String AuthName);
    List<Authority> findAll();

}