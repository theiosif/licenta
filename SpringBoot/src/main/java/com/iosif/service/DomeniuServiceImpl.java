package com.iosif.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iosif.dao.DomeniuDAO;
import com.iosif.entity.Domeniu;

@Service
public class DomeniuServiceImpl implements DomeniuService {


	@Autowired
	private DomeniuDAO domeniuDAO;

	public List<Domeniu> findAllDomenii() {
		return domeniuDAO.findAll();
	}

	public Domeniu updateDomeniu(Domeniu appointment) {
		return domeniuDAO.saveAndFlush(appointment);
	}

	public Domeniu saveDomeniu(Domeniu appointment) {
		return domeniuDAO.save(appointment);
	}

	public void deleteDomeniuById(Short id) {
		domeniuDAO.deleteById(id);
	}
        
        public Domeniu getDomeniu(Short Id){
                return domeniuDAO.getOne(Id);
        }

}





