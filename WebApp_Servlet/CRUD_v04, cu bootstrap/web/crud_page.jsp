<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!--  <title>Planuri de invatamant - main</title>  -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- includem google fonts pt icon-uri si font-uri-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
        <!--includem css-ul creat de noi-->
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

      <nav class="navbar navbar-dark bg-blue navbar-fixed-top">
          <i class="material-icons md-36 md-light ">school</i>
          <!-- in cazul in care iti faci propriul Logo, incluzi numele imaginii la src=""
          <img src=" " width="30" height="30" class="d-inline-block align-top" alt="">
          -->
          <a class="navbar-brand" href="#">Planuri de invatamant actuale [TODO diacritice]</a>
          <form class="form-inline align-items-end">
            <a href="login.jsp" class="btn btn-outline-light" type="submit" role="button">Logout</a>
          </form>
      </nav>

<%@ page import="com.dao.DisciplinaDAO, com.model.*, java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<a href="index.jsp">Inapoi la pagina principala</a>
<br></br>
<h1>CONSOLA ADMIN.</h1>


<%
List<Disciplina> list=DisciplinaDAO.getAllRecords();
request.setAttribute("list",list);
%>


<form name="keyword-search" action="keyword-search" method="GET">
<input type="text" name="keyword" value="" size="30" />
<input type="submit" value="Search" name="keyword_label" />

<table border="1" width="15">
    <thead>
        <tr>
            <th>Domeniu</th>
            <th>Specializare</th>
            <th>An</th>
            <th>Semestru</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <select name="Domeniu">
                    <option></option>
                    <option>ETTI</option>
                    <option>CTI</option>
                </select>
            </td>

            <td>
                <select name="Specializare">
                    <option></option>
                    <option>ELA</option>
                    <option>TST</option>
                    <option>RST</option>
                    <option>CTI</option>
                    <option>ELAen</option>
                    <option>TSTen</option>
                </select>
            </td>
            <td>
                <select name="An">
                    <option></option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>Master1</option>
                    <option>Master2</option>
                    <option>Doctorat</option>
                </select>
            </td>
            <td>
                <select name="Semestru">
                    <option></option>
                    <option>1</option>
                    <option>2</option>
                </select>
            </td>
        </tr>

    </tbody>
</table>
</form>

<!--Admni page table -->
<div class="container center">
  <table class ="table table-hover" style="position:absolute" >

    <thead class ="thead-color">
      <tr>
          <th scope="col">Cod</th>
          <th  scope="col">Domeniu</th>
          <th  scope="col">Specializare</th>
          <th  scope="col">Nume Disciplina</th>
          <th  scope="col">An</th>
          <th  scope="col">Semestru</th>
          <th  scope="col">ECTS</th>
          <th  scope="col">Tip Examinare</th>
          <th  scope="col">Edit</th>
          <th  scope="col">Sterge</th>
      </tr>
    </thead>

    <tbody>
      <c:forEach items="${list}" var="u">
              <tr>
                  <th scope="row">${u.getCod1()}${u.getCod2()}${u.getCod3()}${u.getCod4()}${u.getCod5()}</th>
                  <td>${u.getIdDomeniu()}</td>
                  <td>${u.getIdSpecializare()}</td>
                  <td>${u.getNumeDisciplina()}</td>
                  <td>
                      <fmt:formatNumber value="${(u.getCod3()+1)/2}" maxFractionDigits="0" />
                  </td>
                  <td><c:choose>
                          <c:when test="${u.getCod3()%2!=0}">1</c:when>
                          <c:otherwise>2</c:otherwise>
                      </c:choose>
                  </td>
                  <td>${u.getEcts()}</td>
                  <td>${u.getTipExaminare()}</td>
                  <td><a href="edit?id=<c:out value='${u.getIdDisciplina()}'/>" class="btn btn-link" role="button"><i class="material-icons">border_color</i></td>
                  <td><a href="delete?id=<c:out value='${u.getIdDisciplina()}'/>" class="btn btn-link" role="button"><i class="material-icons">delete</i></td>
              </tr>
      </c:forEach>
  </tbody>
  </table>
</div>
<div>
    <button type="button" class="btn btn-outline-info container center">Adauga nou</button>
</div>

