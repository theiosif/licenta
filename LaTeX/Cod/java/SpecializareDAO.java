package com.iosif.dao;

@Repository
public interface SpecializareDAO extends JpaRepository<Specializare, Short> {

}