package com.model;

import com.model.Disciplina;
import com.model.Specializare;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-05-10T09:54:47")
@StaticMetamodel(Domeniu.class)
public class Domeniu_ { 

    public static volatile CollectionAttribute<Domeniu, Specializare> specializariCollection;
    public static volatile SingularAttribute<Domeniu, Short> idDomeniu;
    public static volatile CollectionAttribute<Domeniu, Disciplina> disciplineCollection;
    public static volatile SingularAttribute<Domeniu, String> codDomeniu;
    public static volatile SingularAttribute<Domeniu, String> numeDomeniu;
    public static volatile SingularAttribute<Domeniu, String> prescurtareD;

}