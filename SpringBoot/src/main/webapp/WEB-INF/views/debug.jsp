<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="DEBUG_FLAG" value="1"/>
<%--<c:if test = "${DEBUG_FLAG}">--%>
<div class="col-md-5 alert alert-info alert-dismissible show mx-auto">
    <button type="button" class="close" data-dismiss="alert">&times;</button>

    <a class="text-left compact">
        <strong><h4>INFO DEPANARE</h4></strong>
    
        <hr/>

        <security:authorize access="isAuthenticated()">
            <span><i> Nume utilizator: <security:authentication property="principal.username"/> </i></span><br>
            <span><i> Roluri: <security:authentication property="principal.authorities"/> </i></span><br>
        </security:authorize>
        <security:authorize access="!isAuthenticated()">
            <span><i> Neautentificat </i><br></span>
        </security:authorize>

        <br>
        <strong>SECURITATE:</strong>
        <a href="gestiune/disciplina/delete?disciplinaId=2">[CSRF] Ștergere</a> |
        <a href="?xss=<script>alert(document.cookie);</script>">[XSS] CSP și antet X-XSS-Protection</a>
    </div>

    <c:if test="${param.xss != null}">

    <div class="col-md-5 alert alert-success alert-dismissible show mx-auto">
        <button type="button" class="close" data-dismiss="alert">&times;</button>

        <div class="text-center compact">
            <strong><h4>XSS nu este executat.</h4></strong>
            <hr/>
            <p><strong>Conținutul cererii:</strong></p>
            <p>${param.xss} [interceptat și înlăturat]</p>

        </div>
    </div>
    </c:if>


</div>


<%--DEBUG--%>