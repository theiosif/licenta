package com.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-05-10T09:54:47")
@StaticMetamodel(Utilizatori.class)
public class Utilizatori_ { 

    public static volatile SingularAttribute<Utilizatori, Integer> idUtilizator;
    public static volatile SingularAttribute<Utilizatori, String> parola;
    public static volatile SingularAttribute<Utilizatori, String> tipUtilizator;
    public static volatile SingularAttribute<Utilizatori, String> nume;

}