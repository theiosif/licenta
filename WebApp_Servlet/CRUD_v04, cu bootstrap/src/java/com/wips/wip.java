/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wips;

import com.model.*;
import com.dao.*;
import java.util.List;
/**
 *
 * @author yoji
 * Throwaway class, 
 * used for code snippet testing only
 */
public class wip {
  
    public static void main(String [] args){
        List<Disciplina> listDisciplina = DisciplinaDAO.getAllRecords();
        System.out.print("\t" + listDisciplina);
    }
}
