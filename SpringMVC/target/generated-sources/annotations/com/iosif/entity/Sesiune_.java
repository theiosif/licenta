package com.iosif.entity;

import com.iosif.entity.Utilizator;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(Sesiune.class)
public class Sesiune_ { 

    public static volatile SingularAttribute<Sesiune, Utilizator> idUtilizator;
    public static volatile SingularAttribute<Sesiune, String> idSesiune;
    public static volatile SingularAttribute<Sesiune, Date> created;

}