package com.iosif.controller;

@Controller
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private DomeniuService domeniuService;

    @Autowired
    private SpecializareService specializareService;

    @Autowired
    private DisciplinaService disciplinaService;

    @Autowired
    private UtilizatorService utilizatorService;
    @PostMapping("/login")
    public String procesareLogin(Model theModel) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilizator user = utilizatorService.findUtilizatorByNume(auth.getName());
        theModel.addAttribute("userName", "Nume Utilizator:" + user.getNume() + " (" + user.getEmail() + ")");
        theModel.addAttribute("adminMessage", "(Pentru Debugging)");

        List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
        theModel.addAttribute("discipline", theDiscipline);

        List<Domeniu> theDomenii = domeniuService.findAllDomenii();
        theModel.addAttribute("domenii", theDomenii);

        List<Specializare> theSpecializari = specializareService.findAllSpecializari();
        theModel.addAttribute("specializari", theSpecializari);

        return "domspec/lista";
    }

    /*@PostMapping("/login")
    public ModelAndView procesareLogin(HttpServletRequest req, RedirectAttributes redir){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/dommspec/lista");


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilizator user = utilizatorService.findUtilizatorByNume(auth.getName());

        redir.addFlashAttribute("userName", "Nume Utilizator:" + user.getNume() + " (" + user.getEmail() + ")");
        redir.addFlashAttribute("adminMessage", "(Pentru Debugging)");

        List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
        redir.addFlashAttribute("discipline", theDiscipline);

        List<Domeniu> theDomenii = domeniuService.findAllDomenii();
        redir.addFlashAttribute("domenii", theDomenii);

        List<Specializare> theSpecializari = specializareService.findAllSpecializari();
        redir.addFlashAttribute("specializari", theSpecializari);

        return mav;
    }*/
}
