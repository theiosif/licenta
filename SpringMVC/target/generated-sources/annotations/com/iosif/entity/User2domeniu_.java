package com.iosif.entity;

import com.iosif.entity.Domeniu;
import com.iosif.entity.Utilizator;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(User2domeniu.class)
public class User2domeniu_ { 

    public static volatile SingularAttribute<User2domeniu, Utilizator> idUtilizator;
    public static volatile SingularAttribute<User2domeniu, Domeniu> idDomeniu;
    public static volatile SingularAttribute<User2domeniu, Long> idDrept;

}