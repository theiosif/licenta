<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<nav class="navbar navbar-dark bg-blue navbar-fixed-top justify-content-between">

    <div class="container-fluid nav-ul"><ul>
    <!-- Logo Propriu? --> <%--<i class="material-icons md-36 md-light ">school</i>--%>
    <li><div class="navbar-brand" href="#">
        <a href="https://upb.ro/"><img src="resources/media/logo_upb_min.png" class="d-inline-block align-top" alt="upb_logo"></a>
        <a href="http://www.electronica.pub.ro"><img src="resources/media/logo_etti_min.png" class="d-inline-block align-top" alt="etti_logo"></a>
    </div></li>


    <li><a href="" class="navbar-brand navbar-text text-white"><h1>Planuri de învățământ</h1></a></li>


    <li>
        <security:authorize access="isAnonymous()">
            <%--Utilizator Neautentificat--%>
            <form class="form-inline align-items-end">
                <button type="button" class="btn btn-outline-light btn-lg" id="myBtn">Login</button>
            </form>
        </security:authorize>

        <security:authorize access="isAuthenticated()">
            <%--Utilizator Autentificat--%>
            <li><form class="form-inline align-items-end" action="gestiune/lista" method="get">
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}" />
                <button type="submit" class="btn nav-admin btn-lg btn-outline-light" ><span class="glyphicon glyphicon-off"></span>Administrare</button>
            </form></li>


            <li><form class="form-inline align-items-end" action="logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}" />
                <button type="submit" class="btn btn-outline-light btn-lg" ><span class="glyphicon glyphicon-off"></span> Logout</button>
            </form></li>
        </security:authorize>
    </li></ul></div>

</nav>

<br>
<br>

<c:if test="${param.eroareLogin != null}">

    <div class="col-md-5 alert alert-danger alert-dismissible show mx-auto">
        <button type="button" class="close" data-dismiss="alert">&times;</button>

        <div class="text-center compact">
            <strong><h4>Eroare de autentificare.</h4></strong>
            <hr/>
            <p><strong>Datele încercării neautorizate au fost înregistrate!</strong></p>
            <p>Verificați-vă datele și încercați din nou.</p>

        </div>
    </div>
</c:if>


<%--DEBUG--%>
<jsp:include page="debug.jsp"></jsp:include>
<hr />
<%--
</c:if>--%>
