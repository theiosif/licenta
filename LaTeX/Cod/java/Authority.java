package com.iosif.entity;

@Entity
@Audited
@Table(name = "role")
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "AUTH_ID")
    private int id;
    @Column(name = "AUTH_NAME")
    private String authName;

    public String getAuthName() {
        return authName;
    }

    public int getId() {
        return id;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public void setId(int id) {
        this.id = id;
    }
}