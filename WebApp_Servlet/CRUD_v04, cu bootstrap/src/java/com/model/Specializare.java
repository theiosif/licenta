/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author yoji
 */
@Entity
@Table(name = "specializari")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Specializari.findAll", query = "SELECT s FROM Specializari s")
    , @NamedQuery(name = "Specializari.findByIdSpecializare", query = "SELECT s FROM Specializari s WHERE s.idSpecializare = :idSpecializare")
    , @NamedQuery(name = "Specializari.findByCodSpecializare", query = "SELECT s FROM Specializari s WHERE s.codSpecializare = :codSpecializare")
    , @NamedQuery(name = "Specializari.findByNumeSpecializare", query = "SELECT s FROM Specializari s WHERE s.numeSpecializare = :numeSpecializare")
    , @NamedQuery(name = "Specializari.findByPrescurtareS", query = "SELECT s FROM Specializari s WHERE s.prescurtareS = :prescurtareS")
    , @NamedQuery(name = "Specializari.findByLbPredare", query = "SELECT s FROM Specializari s WHERE s.lbPredare = :lbPredare")})
public class Specializare implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SPECIALIZARE")
    private Short idSpecializare;
    @Basic(optional = false)
    @Column(name = "COD_SPECIALIZARE")
    private String codSpecializare;
    @Basic(optional = false)
    @Column(name = "NUME_SPECIALIZARE")
    private String numeSpecializare;
    @Basic(optional = false)
    @Column(name = "PRESCURTARE_S")
    private String prescurtareS;
    @Basic(optional = false)
    @Column(name = "LB_PREDARE")
    private String lbPredare;
    @JoinColumn(name = "ID_DOMENIU", referencedColumnName = "ID_DOMENIU")
    @ManyToOne(optional = false)
    private Domeniu idDomeniu;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSpecializare")
    private Collection<Disciplina> disciplineCollection;

    public Specializare() {
    }

    public Specializare(Short idSpecializare) {
        this.idSpecializare = idSpecializare;
    }

    public Specializare(Short idSpecializare, String codSpecializare, String numeSpecializare, String prescurtareS, String lbPredare) {
        this.idSpecializare = idSpecializare;
        this.codSpecializare = codSpecializare;
        this.numeSpecializare = numeSpecializare;
        this.prescurtareS = prescurtareS;
        this.lbPredare = lbPredare;
    }

    public Short getIdSpecializare() {
        return idSpecializare;
    }

    public void setIdSpecializare(Short idSpecializare) {
        this.idSpecializare = idSpecializare;
    }

    public String getCodSpecializare() {
        return codSpecializare;
    }

    public void setCodSpecializare(String codSpecializare) {
        this.codSpecializare = codSpecializare;
    }

    public String getNumeSpecializare() {
        return numeSpecializare;
    }

    public void setNumeSpecializare(String numeSpecializare) {
        this.numeSpecializare = numeSpecializare;
    }

    public String getPrescurtareS() {
        return prescurtareS;
    }

    public void setPrescurtareS(String prescurtareS) {
        this.prescurtareS = prescurtareS;
    }

    public String getLbPredare() {
        return lbPredare;
    }

    public void setLbPredare(String lbPredare) {
        this.lbPredare = lbPredare;
    }

    public Domeniu getIdDomeniu() {
        return idDomeniu;
    }

    public void setIdDomeniu(Domeniu idDomeniu) {
        this.idDomeniu = idDomeniu;
    }

    @XmlTransient
    public Collection<Disciplina> getDisciplineCollection() {
        return disciplineCollection;
    }

    public void setDisciplineCollection(Collection<Disciplina> disciplineCollection) {
        this.disciplineCollection = disciplineCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSpecializare != null ? idSpecializare.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Specializare)) {
            return false;
        }
        Specializare other = (Specializare) object;
        if ((this.idSpecializare == null && other.idSpecializare != null) || (this.idSpecializare != null && !this.idSpecializare.equals(other.idSpecializare))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.controller.Specializari[ idSpecializare=" + idSpecializare + " ]";
    }
    
}
