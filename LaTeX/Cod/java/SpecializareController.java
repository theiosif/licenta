package com.iosif.controller;

@Controller
@RequestMapping("/specializare")
public class SpecializareController {
        @Autowired
	private SpecializareService specializareService;
	
	@GetMapping("/lista")
	public String listaSpecializari(Model theModel) {
		List<Specializare> theSpecializari = specializareService.findAllSpecializari();
		theModel.addAttribute("specializari", theSpecializari);
		return "lista-specializari";
	}
	
	@GetMapping("/showForm")
	public String showFormForAdd(Model theModel) {
		Specializare theSpecializare = new Specializare();
		theModel.addAttribute("specializare", theSpecializare);
		return "specializare-form";
	}
	
	@PostMapping("/saveSpecializare")
	public String saveSpecializare(@ModelAttribute("specializare") Specializare theSpecializare) {
                
                // __NU__ trebuie luate separat ca stringuri ID_Specializare/Specializare
                // si folositi constructorii cu id:
                // theSpecializare.setIdSpecializare(theSpecializare.getIdSpecializare());
                // theSpecializare.setIdSpecializare(theSpecializare.getIdSpecializare());
		specializareService.saveSpecializare(theSpecializare);	
		return "redirect:/specializare/lista";
	}
	
	@GetMapping("/updateForm")
	public String showFormForUpdate(@RequestParam("specializareId") Short theId,
									Model theModel) {
		Specializare theSpecializare = specializareService.getSpecializare(theId);	
		theModel.addAttribute("specializare", theSpecializare);
		return "specializare-form";
	}
	
	@GetMapping("/delete")
	public String deleteSpecializare(@RequestParam("specializareId") Short theId) {
		specializareService.deleteSpecializareById(theId);
		return "redirect:/specializare/lista";
	}
}
