package com.iosif.entity;

import com.iosif.entity.Disciplina;
import com.iosif.entity.Utilizator;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(User2disciplina.class)
public class User2disciplina_ { 

    public static volatile SingularAttribute<User2disciplina, Utilizator> idUtilizator;
    public static volatile SingularAttribute<User2disciplina, Long> idDrept;
    public static volatile SingularAttribute<User2disciplina, Disciplina> idDisciplina;

}