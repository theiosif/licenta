package com.iosif.dao;

@Repository
public interface DisciplinaDAO extends JpaRepository<Disciplina, Integer> {

}