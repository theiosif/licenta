package com.iosif.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.iosif.entity.Disciplina;
import com.iosif.service.DisciplinaService;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/disciplina")
public class DisciplinaController {
        @Autowired
	private DisciplinaService disciplinaService;
	
	@GetMapping("/lista")
	public String listaDiscipline(Model theModel) {
		List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
		theModel.addAttribute("discipline", theDiscipline);
		return "lista-discipline";
	}
	
	@PostMapping("/showForm")
	public String showFormForAdd(Model theModel) {
		Disciplina theDisciplina = new Disciplina();
		theModel.addAttribute("disciplina", theDisciplina);
		return "disciplina-form";
	}
	
	@PostMapping("/saveDisciplina")
	public String saveDisciplina(@ModelAttribute("disciplina") Disciplina theDisciplina) {
                
                // __NU__ trebuie luate separat ca stringuri ID_Disciplina/Specializare
                // si folositi constructorii cu id:
                // theDisciplina.setIdSpecializare(theDisciplina.getIdSpecializare());
                // theDisciplina.setIdDomeniu(theDisciplina.getIdDomeniu());
		disciplinaService.saveDisciplina(theDisciplina);	
		return "redirect:/disciplina/lista";
	}
	
	@PostMapping("/updateForm")
	public String showFormForUpdate(@RequestParam("disciplinaId") Integer theId,
									Model theModel) {
		Disciplina theDisciplina = disciplinaService.getDisciplina(theId);	
		theModel.addAttribute("disciplina", theDisciplina);
		return "disciplina-form";
	}
	
	@PostMapping("/delete")
	public String deleteDisciplina(@RequestParam("disciplinaId") Integer theId) {
		disciplinaService.deleteDisciplinaById(theId);
		return "redirect:/disciplina/lista";
	}
}
