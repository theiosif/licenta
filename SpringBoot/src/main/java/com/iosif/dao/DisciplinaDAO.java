package com.iosif.dao;

import com.iosif.entity.Disciplina;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


/**
 *
 * @author yoji
 */

@Repository
public interface DisciplinaDAO extends JpaRepository<Disciplina, Integer> {

}