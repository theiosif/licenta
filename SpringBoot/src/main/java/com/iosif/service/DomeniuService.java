package com.iosif.service;

import java.util.List;

import com.iosif.entity.Domeniu;

public interface DomeniuService {

	public List<Domeniu> findAllDomenii();
        
	public Domeniu updateDomeniu(Domeniu appointment);
        
	public Domeniu saveDomeniu(Domeniu appointment);
        
	public void deleteDomeniuById(Short id);
        
        public Domeniu getDomeniu(Short Id);
}