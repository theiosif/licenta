package com.iosif.controller;

@Controller
@RequestMapping("/disciplina")
public class DisciplinaController {
        @Autowired
	private DisciplinaService disciplinaService;
	
	@GetMapping("/lista")
	public String listaDiscipline(Model theModel) {
		List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
		theModel.addAttribute("discipline", theDiscipline);
		return "lista-discipline";
	}
	
	@PostMapping("/showForm")
	public String showFormForAdd(Model theModel) {
		Disciplina theDisciplina = new Disciplina();
		theModel.addAttribute("disciplina", theDisciplina);
		return "disciplina-form";
	}
	
	@PostMapping("/saveDisciplina")
	public String saveDisciplina(@ModelAttribute("disciplina") Disciplina theDisciplina) {
                
                // __NU__ trebuie luate separat ca stringuri ID_Disciplina/Specializare
                // si folositi constructorii cu id:
                // theDisciplina.setIdSpecializare(theDisciplina.getIdSpecializare());
                // theDisciplina.setIdDomeniu(theDisciplina.getIdDomeniu());
		disciplinaService.saveDisciplina(theDisciplina);	
		return "redirect:/disciplina/lista";
	}
	
	@PostMapping("/updateForm")
	public String showFormForUpdate(@RequestParam("disciplinaId") Integer theId,
									Model theModel) {
		Disciplina theDisciplina = disciplinaService.getDisciplina(theId);	
		theModel.addAttribute("disciplina", theDisciplina);
		return "disciplina-form";
	}
	
	@PostMapping("/delete")
	public String deleteDisciplina(@RequestParam("disciplinaId") Integer theId) {
		disciplinaService.deleteDisciplinaById(theId);
		return "redirect:/disciplina/lista";
	}
}
