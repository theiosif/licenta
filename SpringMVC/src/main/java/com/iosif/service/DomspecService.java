package com.iosif.service;

import java.util.List;

import com.iosif.entity.Domeniu;
import com.iosif.entity.Specializare;

public interface DomspecService {

	public List<Domeniu> getDomenii();

	public void saveDomeniu(Domeniu theDomeniu);

	public Domeniu getDomeniu(int theId);

	public void deleteDomeniu(int theId);
        
        public List<Specializare> getSpecializari();

	public void saveSpecializare(Specializare theSpecializare);

	public Specializare getSpecializare(int theId);

	public void deleteSpecializare(int theId);
	
}
