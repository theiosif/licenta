package com.iosif.dao;

import com.iosif.entity.Disciplina;
import com.iosif.entity.User2disciplina;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface User2disciplinaDAO extends JpaRepository <User2disciplina, Integer> {
    List<User2disciplina> findByIdUtilizator();

}
