package com.iosif.controller;

@Controller
@RequestMapping("/domspec")
public class DomSpecController {
	@Autowired
	private DomeniuService domeniuService;

	@Autowired
	private SpecializareService specializareService;

	@Autowired
	private DisciplinaService disciplinaService;

	@Autowired
	private UtilizatorService utilizatorService;

	@GetMapping("/lista")
	public String listaDiscipline(Model theModel) {

		List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
		theModel.addAttribute("discipline", theDiscipline);

		List<Domeniu> theDomenii = domeniuService.findAllDomenii();
		theModel.addAttribute("domenii", theDomenii);

		List<Specializare> theSpecializari = specializareService.findAllSpecializari();
		theModel.addAttribute("specializari", theSpecializari);

		return "lista-domspec";
	}
	

}
