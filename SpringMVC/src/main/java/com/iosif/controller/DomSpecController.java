package com.iosif.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.iosif.entity.Domeniu;
import com.iosif.entity.Specializare;
import com.iosif.service.DomspecService;

import com.iosif.entity.Disciplina;
import com.iosif.service.DisciplinaService;
import org.springframework.web.bind.annotation.RestController;


@Controller
@RequestMapping("/domspec")
public class DomSpecController {
        @Autowired
	private DomspecService domspecService;
        @Autowired
        private DisciplinaService disciplinaService;
	
	@GetMapping("/lista")
	public String listaDiscipline(Model theModel) {
		
                List<Disciplina> theDiscipline = disciplinaService.getDiscipline();
                theModel.addAttribute("discipline", theDiscipline);
            
                List<Domeniu> theDomenii = domspecService.getDomenii();
		theModel.addAttribute("domenii", theDomenii);
                
                List<Specializare> theSpecializari = domspecService.getSpecializari();
		theModel.addAttribute("specializari", theSpecializari);
		
                return "lista-domspec";
	}
	
        
	@GetMapping("/showFormDomeniu")
	public String showFormForAddDom(Model theModel) {
		Domeniu theDomeniu = new Domeniu();
		theModel.addAttribute("domspec", theDomeniu);
		return "domeniu-form";
	}
        
        @GetMapping("/showFormSpecializare")
	public String showFormForAddSpec(Model theModel) {
		Specializare theSpecializare = new Specializare();
		theModel.addAttribute("domspec", theSpecializare);
		return "specializare-form";
	}
	
	@PostMapping("/saveDomeniu")
	public String saveDomeniu(@ModelAttribute("domenii") Domeniu theDomeniu) {
		domspecService.saveDomeniu(theDomeniu);	
		return "redirect:/domspec/lista";
	}
        
        @PostMapping("/saveSpecializare")
	public String saveSpecializare(@ModelAttribute("specializari") Specializare theSpecializare) {
                domspecService.saveSpecializare(theSpecializare);	
		return "redirect:/domspec/lista";
	}
	
       
	@GetMapping("/updateFormDomeniu")
	public String showFormForUpdateDomeniu(@RequestParam("domeniuId") int theId,
									Model theModel) {
		Domeniu theDomeniu = domspecService.getDomeniu(theId);	
		theModel.addAttribute("domeniu", theDomeniu);
		return "domeniu-form";
	}
        
        @GetMapping("/updateFormSpecializare")
	public String showFormForUpdateSpecializare(@RequestParam("specializareId") int theId,
									Model theModel) {
		Specializare theSpecializare = domspecService.getSpecializare(theId);	
		theModel.addAttribute("specializare", theSpecializare);
		return "specializare-form";
	}
	
	@GetMapping("/deleteDomeniu")
	public String deleteDomeniu(@RequestParam("domeniuId") int theId) {
		domspecService.deleteDomeniu(theId);
		return "redirect:/domspec/lista";
	}
        
        
        @GetMapping("/deleteSpecializare")
	public String deleteSpecializare(@RequestParam("specializareId") int theId) {
		domspecService.deleteSpecializare(theId);
		return "redirect:/domspec/lista";
	}
}
