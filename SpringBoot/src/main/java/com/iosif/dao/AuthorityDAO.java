package com.iosif.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iosif.entity.Authority;

import java.util.List;

@Repository
public interface AuthorityDAO extends JpaRepository<Authority, Integer> {
    Authority findByAuthName(String AuthName);
    List<Authority> findAll();

}