
package com.iosif.controller;

import com.iosif.dao.LoginDAO;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */

// TODO: n-are de ce sa fie clasa separata, trb mutat in AuthController
@WebServlet("/LogoutController")
public class LogoutController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        
//        response.setContentType("text/html");
//    	
//        // parse cookies to get session_id
//        String session_id = "";
//        Cookie[] cookies = request.getCookies();
//    	if(cookies != null){
//            for(Cookie cookie : cookies){
//                    if(cookie.getName().equals("JSESSIONID")){
//                            System.out.println("JSESSIONID="+cookie.getValue());
//                    }
//                    if(cookie.getName().equals("id")){
//                            session_id = cookie.getValue();
//                            System.out.println("id= "+session_id);
//                            cookie.setValue("");
//                            cookie.setMaxAge(0);
//                            response.addCookie(cookie);
//                    }
//                    
//            }
//    	}
//        
//        // do actual removal
//        LoginDAO login_dao = new LoginDAO();
//        System.out.println("SID_ControllerLogout="+session_id+"|DONE");
//        login_dao.del_session(session_id);
//        
//    	//invalidate the session if exists
//    	HttpSession session = request.getSession(false);
//    	System.out.println("id="+session.getAttribute("auth_type"));
//    	if(session != null){
//    		session.invalidate();
//    	}
//    	response.sendRedirect("index.jsp");
//    }

}
