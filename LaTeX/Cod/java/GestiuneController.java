package com.iosif.controller;

@Controller
@RequestMapping("/gestiune")
public class GestiuneController {


    @Autowired
    private DomeniuService domeniuService;
    @Autowired
    private SpecializareService specializareService;
    @Autowired
    private DisciplinaService disciplinaService;
    @Autowired
    private UtilizatorService utilizatorService;

    @GetMapping("/lista")
    public String listaDiscipline(Model theModel) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilizator user = utilizatorService.findUtilizatorByNume(auth.getName());
        theModel.addAttribute("userName", "Nume Utilizator:" + user.getNume() + " (" + user.getEmail() + ")");
        theModel.addAttribute("adminMessage", "(Pentru Debugging)");

        List<Disciplina> theDiscipline = disciplinaService.findAllDiscipline();
        theModel.addAttribute("discipline", theDiscipline);

        List<Domeniu> theDomenii = domeniuService.findAllDomenii();
        theModel.addAttribute("domenii", theDomenii);

        List<Specializare> theSpecializari = specializareService.findAllSpecializari();
        theModel.addAttribute("specializari", theSpecializari);

        return "gestiune";
    }

    @PostMapping("/domeniu/showForm")
    public String showFormForAddDomeniu(Model theModel) {
        Domeniu theDomeniu = new Domeniu();
        theModel.addAttribute("domeniu", theDomeniu);
        return "domeniu-form";
    }

    @PostMapping("/domeniu/saveDomeniu")
    public String saveDomeniu(@ModelAttribute("domeniu") Domeniu theDomeniu) {

        // __NU__ trebuie luate separat ca stringuri ID_Domeniu/Domeniu
        // si folositi constructorii cu id:
        // theDomeniu.setIdDomeniu(theDomeniu.getIdDomeniu());
        // theDomeniu.setIdDomeniu(theDomeniu.getIdDomeniu());
        domeniuService.saveDomeniu(theDomeniu);
        return "redirect:/gestiune/lista";
    }

    @PostMapping("/domeniu/updateForm")
    public String showFormForUpdateDomeniu(@RequestParam("domeniuId") Short theId,
                                    Model theModel) {
        Domeniu theDomeniu = domeniuService.getDomeniu(theId);
        theModel.addAttribute("domeniu", theDomeniu);
        return "domeniu-form";
    }

    @PostMapping("/domeniu/delete")
    public String deleteDomeniu(@RequestParam("domeniuId") Short theId) {
        domeniuService.deleteDomeniuById(theId);
        return "redirect:/gestiune/lista";
    }
    
    
    @PostMapping("/specializare/showForm")
    public String showFormForAddSpecializare(Model theModel) {
        Specializare theSpecializare = new Specializare();
        theModel.addAttribute("specializare", theSpecializare);
        return "specializare-form";
    }

    @PostMapping("/specializare/saveSpecializare")
    public String saveSpecializare(@ModelAttribute("specializare") Specializare theSpecializare) {

        // __NU__ trebuie luate separat ca stringuri ID_Specializare/Specializare
        // si folositi constructorii cu id:
        // theSpecializare.setIdSpecializare(theSpecializare.getIdSpecializare());
        // theSpecializare.setIdSpecializare(theSpecializare.getIdSpecializare());
        specializareService.saveSpecializare(theSpecializare);
        return "redirect:/gestiune/lista";
    }

    @PostMapping("/specializare/updateForm")
    public String showFormForUpdateSpecializare(@RequestParam("specializareId") Short theId,
                                    Model theModel) {
        Specializare theSpecializare = specializareService.getSpecializare(theId);
        theModel.addAttribute("specializare", theSpecializare);
        return "specializare-form";
    }

    @PostMapping("/specializare/delete")
    public String deleteSpecializare(@RequestParam("specializareId") Short theId) {
        specializareService.deleteSpecializareById(theId);
        return "redirect:/gestiune/lista";
    }


    @PostMapping("/disciplina/showForm")
    public String showFormForAddDisciplina(Model theModel) {
        Disciplina theDisciplina = new Disciplina();
        theModel.addAttribute("disciplina", theDisciplina);
        return "disciplina-form";
    }

    @PostMapping("/disciplina/saveDisciplina")
    public String saveDisciplina(@ModelAttribute("disciplina") Disciplina theDisciplina) {

        // __NU__ trebuie luate separat ca stringuri ID_Disciplina/Disciplina
        // si folositi constructorii cu id:
        // theDisciplina.setIdDisciplina(theDisciplina.getIdDisciplina());
        // theDisciplina.setIdDisciplina(theDisciplina.getIdDisciplina());
        disciplinaService.saveDisciplina(theDisciplina);
        return "redirect:/gestiune/lista";
    }

    @PostMapping("/disciplina/updateForm")
    public String showFormForUpdateDisciplina(@RequestParam("disciplinaId") Integer theId,
                                                Model theModel) {
        Disciplina theDisciplina = disciplinaService.getDisciplina(theId);
        theModel.addAttribute("disciplina", theDisciplina);
        return "disciplina-form";
    }

    @PostMapping("/disciplina/delete")
    public String deleteDisciplina(@RequestParam("disciplinaId") Integer theId) {
        disciplinaService.deleteDisciplinaById(theId);
        return "redirect:/gestiune/lista";
    }


}
