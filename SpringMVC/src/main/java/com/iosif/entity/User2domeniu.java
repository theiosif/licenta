/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iosif.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yoji
 */
@Entity
@Table(name = "user2domeniu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User2domeniu.findAll", query = "SELECT u FROM User2domeniu u"),
    @NamedQuery(name = "User2domeniu.findByIdDrept", query = "SELECT u FROM User2domeniu u WHERE u.idDrept = :idDrept")})
public class User2domeniu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DREPT")
    private Long idDrept;
    @JoinColumn(name = "ID_UTILIZATOR", referencedColumnName = "ID_UTILIZATOR")
    @ManyToOne
    private Utilizator idUtilizator;
    @JoinColumn(name = "ID_DOMENIU", referencedColumnName = "ID_DOMENIU")
    @ManyToOne
    private Domeniu idDomeniu;

    public User2domeniu() {
    }

    public User2domeniu(Long idDrept) {
        this.idDrept = idDrept;
    }

    public Long getIdDrept() {
        return idDrept;
    }

    public void setIdDrept(Long idDrept) {
        this.idDrept = idDrept;
    }

    public Utilizator getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(Utilizator idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public Domeniu getIdDomeniu() {
        return idDomeniu;
    }

    public void setIdDomeniu(Domeniu idDomeniu) {
        this.idDomeniu = idDomeniu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDrept != null ? idDrept.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User2domeniu)) {
            return false;
        }
        User2domeniu other = (User2domeniu) object;
        if ((this.idDrept == null && other.idDrept != null) || (this.idDrept != null && !this.idDrept.equals(other.idDrept))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.User2domeniu[ idDrept=" + idDrept + " ]";
    }
    
}
