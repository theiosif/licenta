package com.iosif.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.iosif.entity.Domeniu;
import com.iosif.service.DomeniuService;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/domeniu")
public class DomeniuController {
        @Autowired
	private DomeniuService domeniuService;
	
	@GetMapping("/lista")
	public String listaDomenii(Model theModel) {
		List<Domeniu> theDomenii = domeniuService.findAllDomenii();
		theModel.addAttribute("domenii", theDomenii);
		return "lista-domenii";
	}
	
	@GetMapping("/showForm")
	public String showFormForAdd(Model theModel) {
		Domeniu theDomeniu = new Domeniu();
		theModel.addAttribute("domeniu", theDomeniu);
		return "domeniu-form";
	}
	
	@PostMapping("/saveDomeniu")
	public String saveDomeniu(@ModelAttribute("domeniu") Domeniu theDomeniu) {
                
                // __NU__ trebuie luate separat ca stringuri ID_Domeniu/Specializare
                // si folositi constructorii cu id:
                // theDomeniu.setIdSpecializare(theDomeniu.getIdSpecializare());
                // theDomeniu.setIdDomeniu(theDomeniu.getIdDomeniu());
		domeniuService.saveDomeniu(theDomeniu);	
		return "redirect:/domeniu/lista";
	}
	
	@GetMapping("/updateForm")
	public String showFormForUpdate(@RequestParam("domeniuId") Short theId,
									Model theModel) {
		Domeniu theDomeniu = domeniuService.getDomeniu(theId);	
		theModel.addAttribute("domeniu", theDomeniu);
		return "domeniu-form";
	}
	
	@GetMapping("/delete")
	public String deleteDomeniu(@RequestParam("domeniuId") Short theId) {
		domeniuService.deleteDomeniuById(theId);
		return "redirect:/domeniu/lista";
	}
}
