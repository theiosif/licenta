/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iosif.entity;

import org.hibernate.envers.Audited;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yoji
 */
@Entity
@Audited
@Table(name = "user2disciplina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User2disciplina.findAll", query = "SELECT u FROM User2disciplina u"),
    @NamedQuery(name = "User2disciplina.findByIdDrept", query = "SELECT u FROM User2disciplina u WHERE u.idDrept = :idDrept"),
    @NamedQuery(name = "User2disciplina.findByIdUtilizator", query = "SELECT u FROM User2disciplina u WHERE u.idUtilizator = :idUtilizator")})
public class User2disciplina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DREPT")
    private Long idDrept;
    @JoinColumn(name = "ID_UTILIZATOR", referencedColumnName = "ID_UTILIZATOR")
    @ManyToOne
    private Utilizator idUtilizator;
    @JoinColumn(name = "ID_DISCIPLINA", referencedColumnName = "ID_DISCIPLINA")
    @ManyToOne
    private Disciplina idDisciplina;

    public User2disciplina() {
    }

    public User2disciplina(Long idDrept) {
        this.idDrept = idDrept;
    }

    public Long getIdDrept() {
        return idDrept;
    }

    public void setIdDrept(Long idDrept) {
        this.idDrept = idDrept;
    }

    public Utilizator getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(Utilizator idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public Disciplina getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Disciplina idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDrept != null ? idDrept.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User2disciplina)) {
            return false;
        }
        User2disciplina other = (User2disciplina) object;
        if ((this.idDrept == null && other.idDrept != null) || (this.idDrept != null && !this.idDrept.equals(other.idDrept))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iosif.entity.User2disciplina[ idDrept=" + idDrept + " ]";
    }
    
}
