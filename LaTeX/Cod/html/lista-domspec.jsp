<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

<!DOCTYPE html>
<html>
<head>
	<base href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">
	<!-- Required meta tags -->
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>[UPB] Lista Disciplinelor</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<!-- includem google fonts pt icon-uri si font-uri-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- CSS PROPRIU-ish  XX ATENTIE RENAME -->
	<link href="<c:url value="/resources/css/style.css" />"	rel="stylesheet">
</head>


<body>
<%@include file="header.jsp" %>

<div class="container">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active " id="discipline-tab" data-toggle="tab" href="#discipline" role="tab" aria-controls="discipline" aria-selected="true">Discipline</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " id="domenii-tab" data-toggle="tab" href="#domenii" role="tab" aria-controls="domenii" aria-selected="false">Domenii</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " id="specializari-tab" data-toggle="tab" href="#specializari" role="tab" aria-controls="specializari" aria-selected="false">Specializari</a>
        </li>
    </ul>
</div>

<div class="tab-content" id="myTabContent">

<!-- TAB DISCIPLINE -->
<div class="tab-pane fade show active" id="discipline" role="tabpanel" aria-labelledby="discipline-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />
			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Disciplinelor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th class="bg-blue text-white text-center"><h5>Cod</h5></th>
							<th class="bg-blue text-white text-center"><h5>Domeniu</h5></th>
							<th class="bg-blue text-white text-center"><h5>Specializare</h5></th>
							<th class="bg-blue text-white text-center"><h5>Nume Disciplina</h5></th>


                        </tr>

						<!-- loop over and print our discipline -->
						<c:forEach var="tempDisciplina" items="${discipline}">
                            

							<tr>
								<td>${tempDisciplina.cod1}${tempDisciplina.cod2}${tempDisciplina.cod3}${tempDisciplina.cod4}${tempDisciplina.cod5}</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idDomeniu.numeDomeniu}">${tempDisciplina.idDomeniu.prescurtareD}</a>
							 	</td>

								<td>
									<a href="#" rel="tooltip" title="${tempDisciplina.idSpecializare.numeSpecializare}">${tempDisciplina.idSpecializare.prescurtareS}</a>
								</td>

								<td>${tempDisciplina.numeDisciplina}</td>

						</tr>

					</c:forEach>

				</table>

				</div>
			</div>
		</div>
	</div>
</div> <!-- end div disicpline -->


<!-- TAB DOMENII -->
<div class="tab-pane fade" id="domenii" role="tabpanel" aria-labelledby="domenii-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />

			<div class="card card-info">
				<div class="card-header">
					<div class="card-title h2">Lista Domeniilor</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th class="bg-blue text-white text-center"><h5>Cod</h5></th>
							<th class="bg-blue text-white text-center"><h5>Prescurtare</h5></th>
							<th class="bg-blue text-white text-center"><h5>Nume</h5></th>

						</tr>

						<c:forEach var="tempDomeniu" items="${domenii}">

							<!-- constructie link-uri domnii -->
							<c:url var="updateLink" value="/gestiune/domeniu/updateForm">
								<c:param name="domeniuId" value="${tempDomeniu.idDomeniu}" />
							</c:url>
							<c:url var="deleteLink" value="/gestiune/domeniu/delete">
								<c:param name="domeniuId" value="${tempDomeniu.idDomeniu}" />
							</c:url>

							<tr>
								<td>${tempDomeniu.codDomeniu}</td>

								<td>${tempDomeniu.prescurtareD}</td>

								<td>${tempDomeniu.numeDomeniu}</td>

							</td>

							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>
	</div>
</div> <!-- end div domenii -->

<!-- TAB SPECIALIZARI -->
<div class="tab-pane fade" id="specializari" role="tabpanel" aria-labelledby="specializari-tab">
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<hr />

			<div class="card card-info">
                <div class="card-header">
					<div class="card-title h2">Lista Specializarilor</div>
				</div>

				<div class="card-body">
                    <div class="container">
                        <!-- loop pentru domenii // constructie tab headers -->
                        <ul class="nav nav-tabs" id="TabHeaderDomenii" role="tablist">
                            <c:forEach var="tempDomeniu" items="${domenii}" varStatus = "status">
                            <li ${status.first ? 'class="active"' : ''}>
                                <a data-toggle="tab" class="nav-link " href="#${tempDomeniu.prescurtareD}">${tempDomeniu.prescurtareD}</a>
                            </li>
                            </c:forEach>
                        </ul>
					 </div>


					<div class="tab-content" id="TabContentDomenii">

					<!-- outer loop pentru domenii // constructie tab content -->
                    <c:forEach var="tempDomeniu" items="${domenii}" varStatus = "status">
                        <div class="tab-pane fade ${status.first ? 'show active' : ''}" id="${tempDomeniu.prescurtareD}">
                            <div class="container">
                                <div class="col-md-offset-1 col-md-10">
                                    <hr />
                                    <table class="table table-striped table-bordered">
        								<tr>
        									<th class="bg-blue text-white text-center"><h5>Cod</h5></th>
        									<th class="bg-blue text-white text-center"><h5>Prescurtare</h5></th>
        									<th class="bg-blue text-white text-center"><h5>Nume</h5></th>
        								</tr>

        								<!-- inner loop pentru specializari -->
        								<c:forEach var="tempSpecializare" items="${tempDomeniu.specializariCollection}">


        									<tr>
        										<td>${tempSpecializare.codSpecializare}</td>
        										<td>${tempSpecializare.prescurtareS}</td>
        										<td>${tempSpecializare.numeSpecializare}</td>

        									</tr>

                                        </c:forEach> <!-- endloop dom.specializari -->
                                    </table>
                                </div>
                            </div>
                        </div>
					</c:forEach> <!-- endloop domenii -->
                    </div> <!-- End of Tab Content -->

                </div> <!-- end of card-body -->
            </div> <!-- end of card card-info -->
        </div> <!-- end of col whatever -->
    </div> <!-- end of container -->
</div> <!-- end of tab pane SPECIALIZARI -->

</div> <!-- end tab content Parent -->

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/tooltip.js"></script>
<jsp:include page="modal-login.jsp"></jsp:include>
</body>
</html>
