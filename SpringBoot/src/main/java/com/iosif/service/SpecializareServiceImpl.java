package com.iosif.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iosif.dao.SpecializareDAO;
import com.iosif.entity.Specializare;

@Service
public class SpecializareServiceImpl implements SpecializareService {


	@Autowired
	private SpecializareDAO specializareDAO;

	public List<Specializare> findAllSpecializari() {
		return specializareDAO.findAll();
	}

	public Specializare updateSpecializare(Specializare appointment) {
		return specializareDAO.saveAndFlush(appointment);
	}

	public Specializare saveSpecializare(Specializare appointment) {
		return specializareDAO.save(appointment);
	}

	public void deleteSpecializareById(Short id) {
		specializareDAO.deleteById(id);
	}
        
        public Specializare getSpecializare(Short Id){
                return specializareDAO.getOne(Id);
        }

}





