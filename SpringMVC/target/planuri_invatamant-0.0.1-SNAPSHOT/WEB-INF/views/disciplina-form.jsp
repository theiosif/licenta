<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modifică Disciplină</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

</head>
<body>
	<div class="container">
		<div class="col-md-offset-2 col-md-7">
			<h2 class="text-center">Modifică Disciplină</h2>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Formular Disciplină</div>
				</div>
				<div class="panel-body">
					<form:form action="saveDisciplina" cssClass="form-horizontal"
						method="post" modelAttribute="disciplina">

						<!-- need to associate this data with customer id -->
						<form:hidden path="idDisciplina" />

						<div class="form-group">
							<label for="iddomeniu" class="col-md-3 control-label">Domeniu</label>
							<div class="col-md-9">
								<form:input path="idDomeniu.idDomeniu" cssClass="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label for="idspecializare" class="col-md-3 control-label">Specializare</label>
							<div class="col-md-9">
								<form:input path="idSpecializare.idSpecializare" cssClass="form-control" />
							</div>
						</div>
                                                <div class="form-group">
							<label for="cod1" class="col-md-3 control-label">Cod1</label>
							<div class="col-md-9">
								<form:input path="cod1" cssClass="form-control" />
							</div>
						</div>
                                                <div class="form-group">
							<label for="cod2" class="col-md-3 control-label">cod2</label>
							<div class="col-md-9">
								<form:input path="cod2" cssClass="form-control" />
							</div>
						</div>
                                                <div class="form-group">
							<label for="cod3" class="col-md-3 control-label">Cod1</label>
							<div class="col-md-9">
								<form:input path="cod3" cssClass="form-control" />
							</div>
						</div>
                                                <div class="form-group">
							<label for="cod4" class="col-md-3 control-label">cod4</label>
							<div class="col-md-9">
								<form:input path="cod4" cssClass="form-control" />
							</div>
						</div>
                                                <div class="form-group">
							<label for="cod5" class="col-md-3 control-label">cod5</label>
							<div class="col-md-9">
								<form:input path="cod5" cssClass="form-control" />
							</div>
						</div>
                                                <div class="form-group">
							<label for="lbPredare" class="col-md-3 control-label">lbPredare</label>
							<div class="col-md-9">
								<form:input path="lbPredare" cssClass="form-control" />
							</div>
						</div>
                                                

						<div class="form-group">
							<label for="numeDisciplina" class="col-md-3 control-label">Nume</label>
							<div class="col-md-9">
								<form:input path="numeDisciplina" cssClass="form-control" />
							</div>
						</div>

						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<form:button cssClass="btn btn-primary">Confirmă</form:button>
							</div>
						</div>

					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>