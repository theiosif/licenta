package com.iosif.dao;

import com.iosif.entity.Specializare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author yoji
 */

@Repository
public interface SpecializareDAO extends JpaRepository<Specializare, Short> {

}