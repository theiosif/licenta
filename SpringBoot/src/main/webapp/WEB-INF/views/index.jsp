<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--<jsp:include page="head-meta.jsp"></jsp:include>--%>
<!DOCTYPE html>
<html>

<%@include file="head-meta.jsp" %>



<body>

    <jsp:include page="header.jsp"></jsp:include>

    <div class="container">
      <div class="card-deck mb-6 text-center">

        <div class="card mb-6 box-shadow">
          <div class="card-header">
            <h3 class="my-0 font-weight-normal">Discipline</h3>
          </div>
          <div class="card-body d-flex flex-column">
            <h4 class="card-title pricing-card-title">Planul curent de învățământ</h4>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Detalii per disciplină</li>
              <li><small class="text-muted">ECTS/ repartizarea orelor/ limbă predare, etc.</small></li>
            </ul>
			<p><a class="btn btn-lg bg-blue btn-primary mt-auto align-bottom" href="disciplina/lista" role="button">
				Discipline &raquo;
			</a></p>
          </div>
        </div>

        <div class="card mb-6 box-shadow">
          <div class="card-header">
			<h3 class="my-0 font-weight-normal">Oferte de Studiu</h3>
		  </div>

          <div class="card-body d-flex flex-column">
            <h4 class="card-title pricing-card-title">Domeniile și specializările acestora</h4>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Lista domeniilor/ specializărilor</li>
              <li><small class="text-muted">Descrieri sumare</small></li>
            </ul>
			<p><a class="btn btn-lg bg-blue btn-primary mt-auto align-bottom" href="domspec/lista" role="button">
				Domenii & Specializări &raquo;
			</a></p>
          </div>
        </div>

      </div>

<!-- Modal Login -->
<jsp:include page="modal-login.jsp"></jsp:include>
</body>

</html>
