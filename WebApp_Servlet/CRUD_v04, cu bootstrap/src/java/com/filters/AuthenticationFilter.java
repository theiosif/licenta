/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filters;

/**
 *
 * @author yoji
 */

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
public class CookieFilter implements GenericFilterBean {
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    Cookie[] cookies = request.getCookies();
    Stream<Cookie> stream = Objects.nonNull(cookies) ? Arrays.stream(cookies) : Stream.empty();
    String cookieValue = stream.filter(cookie -> "nameOfMyCookie".equals(cookie.getName()))
        .findFirst()
        .orElse(new Cookie("nameOfMyCookie", null))
        .getValue();
    if (Objects.nonNull(cookieValue)) {
      request.setAttribute("myCoolObject", myObject);
    } 
    chain.doFilter(request, res);
  }
*/


@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {

	private ServletContext context;
	
	public void init(FilterConfig fConfig) throws ServletException {
		this.context = fConfig.getServletContext();
		this.context.log("AuthenticationFilter initialized");
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		String uri = req.getRequestURI();
		this.context.log("Requested Resource::"+uri);
		
		Cookie[] cookies;
                cookies = req.getCookies();
                Stream<Cookie> stream = Objects.nonNull(cookies) ? Arrays.stream(cookies) : Stream.empty();
                String cookieValue = stream.filter(cookie -> "id".equals(cookie.getName()))
                    .findFirst()
                    .orElse(new Cookie("id", null))
                    .getValue();
                
                // debug_flag
                boolean debug_flag = true;
                if (debug_flag){
                    this.context.log("\n\n\n\n\nURI="+uri);

                    if (Objects.nonNull(cookieValue))
                        this.context.log("cookie="+cookieValue+"\n\n\n\n\n");
                    else
                       this.context.log("COOKIE NULL\n\n\n\n\n");
                }
                
		if(Objects.isNull(cookieValue) && uri.startsWith("/CRUD_v01/crud")){
			this.context.log("Unauthorized access request");
			res.sendRedirect("login.jsp");
                        
		}else{
                        this.context.log("HERE(1.1)");
			// pass the request along the filter chain
			chain.doFilter(request, response);
		}

	}

	public void destroy() {
		//close any resources here
	}

}
