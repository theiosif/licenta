<%--
    Document   : login
    Created on : Dec 5, 2018, 12:47:52 PM
    Author     : yoji
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
  <meta charset="ISO-8859-1">
  <title></title>
  <!--STYLE-->

  <!--includem css-ul din bootstrap-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <!-- includem google fonts pt icon-uri si font-uri-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
    rel="stylesheet">
  <!--includem css-ul creat de noi-->
  <link rel="stylesheet" href="login.css">
</head>

<nav class="navbar navbar-dark bg-blue navbar-fixed-top">
    <i class="material-icons md-36 md-light ">school</i>
    <!-- Logo Propriu?
    <img src=" " width="30" height="30" class="d-inline-block align-top" alt="">
    -->
    <a class="navbar-brand" href="vizualizare.jsp">Planuri de invatamant actuale</a>
    <form class="form-inline align-items-end">
      <a href="login.jsp" class="btn btn-outline-light" type="submit" role="button">Log in</a>
    </form>
</nav>


<body>
  <div class="container">
    <div class="d-flex justify-content-center h-100">

      <div class="card">

        <div class="card-header">
          <h3>Autenficare Administrator</h3>
        </div>

        <div class="card-body">

          <form name="request_auth" action="request_auth" method="get">
            <div class="input-group form-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="text" name="user" class="form-control" placeholder="username">
            </div>

            <div class="input-group form-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" name="parola" class="form-control" placeholder="parola">
            </div>

            <div class="form-group">
              <input type="submit" value="Login" class="btn float-right login_btn">
            </div>
          </form>

        </div>

      </div>
    </div>
  </div>
</body>
</html>
