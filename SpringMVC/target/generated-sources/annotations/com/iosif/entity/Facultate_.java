package com.iosif.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-20T15:20:32")
@StaticMetamodel(Facultate.class)
public class Facultate_ { 

    public static volatile SingularAttribute<Facultate, Short> codFac;
    public static volatile SingularAttribute<Facultate, String> numeFac;
    public static volatile SingularAttribute<Facultate, Short> idFac;
    public static volatile SingularAttribute<Facultate, String> prescurtareFac;

}